#!/bin/bash
#
# Slurm job options (name, compute nodes, job time)
#SBATCH --job-name=bopt_nn
#SBATCH --time=96:00:0
#SBATCH --partition=gpu-cascade
#SBATCH --qos=gpu
#SBATCH --gres=gpu:1
#SBATCH --output=bopt_nn_3.out     # file to collect standard output
#SBATCH --error=bopt_nn_3.err      # file to collect standard errors

# Budget code (e.g. t01)
#SBATCH --account=ec180

# Load the required modules
source /work/ec180/ec180/esedov/miniconda-init.sh

python3 nn_bopt.py

conda deactivate
