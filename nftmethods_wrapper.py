import ctypes
import numpy as np
from numpy import ctypeslib


def get_lib_path():
    """Return the path of the FNFT file.

    Edit this function to set the location of the compiled library for nftmethods.
    See example strings below.

    Returns:

    * libstring : string holding library path

    Example paths:

        * libstr = "C:/Libraries/local/libnftmethods.dll"  # example for windows
        * libstr = "/usr/local/lib/libnftmethods.so"  # example for linux

    """
    #libstr = "/usr/local/lib/libnftmethods.so"
    libstr = "/home/esf0/CLionProjects/nftmethods/cmake-build-release/src/libnftmethods_shared.so"

    return libstr


def some_function(one, two):

    nft_clib = ctypes.CDLL(get_lib_path())

    clib_func = nft_clib.IsArgJump
    clib_func.restype = ctypes.c_int

    clib_func.argtypes = [
        np.ctypeslib.ndpointer(dtype=np.complex128, ndim=1, flags='C'),
        np.ctypeslib.ndpointer(dtype=np.complex128, ndim=1, flags='C')
    ]

    return clib_func(one, two)


if __name__ == "__main__":

    one = 1+1j
    two = -1+1j
    print(some_function(one, two))