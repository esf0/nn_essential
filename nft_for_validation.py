from FNFTpy import nsev
import numpy as np
import timeit
from tqdm import tqdm


def get_metric_old(init_real, init_imag, predict_real, predict_imag):
    mean = np.mean(np.power(np.absolute((init_real - predict_real) + 1.0j * (init_imag - predict_imag)), 2))
    # mean = np.mean(np.absolute((init_real - predict_real) / init_real + 1.0j * (init_imag - predict_imag) / init_imag)) # this is very big for small values

    return mean


def get_metric(init_real, init_imag, predict_real, predict_imag):
    mean = np.mean(np.absolute((init_real - predict_real) + 1.0j * (init_imag - predict_imag))) / np.mean(
        np.absolute(init_real + 1.0j * init_imag))
    # mean = np.mean(np.absolute((init_real - predict_real) / init_real + 1.0j * (init_imag - predict_imag) / init_imag)) # this is very big for small values

    return mean


# def get_metric_normalized(init_real, init_imag, predict_real, predict_imag):
#     mean_real, mean_imag = get_metric(init_real, init_imag, predict_real, predict_imag)
#
#     return mean_real / np.mean(init_real), mean_imag / np.mean(init_imag)


t_span = 1.
n_span = 1024
dt = t_span / n_span
t = np.linspace(-t_span / 2., t_span / 2. - dt, n_span)

xi_span = np.pi / (2. * dt)
dxi = xi_span / n_span
xi = np.linspace(-xi_span / 2., xi_span / 2. - dxi, n_span)

noise_level_range = [9999, 0, 5, 10, 13, 17, 20, 25, 30]
# noise_level_range = [9999, 0, 5]
metric_values = []
metric_values_old = []

init_spec = []
init_spec_b = []


for noise_level in noise_level_range:
    tic = timeit.default_timer()

    # test_data = np.load("/home/esf0/PycharmProjects/nn_training/data/test_data_cont_spec_wdm_wo_sol_nl_"
    #                     + str(noise_level) + ".npy", allow_pickle=True)
    test_data = np.load("/home/esf0/PycharmProjects/nn_training/data/test_data_cont_spec_b_wdm_wo_sol_nl_"
                        + str(noise_level) + ".npy", allow_pickle=True)
    print("Training data loaded: SNR =", noise_level, "; size =", len(test_data))

    test_signal = test_data[0] + 1.0j * test_data[1]
    test_spec = test_data[2] + 1.0j * test_data[3]
    # N_VAL = len(test_signal)
    N_VAL = 1000

    # if noise_level == 9999:
    #     for ind in range(N_VAL):
    #         q = test_signal[ind]
    #         res = nsev(q, t, M=n_span, Xi1=-xi_span / 2., Xi2=xi_span / 2. - dxi, bsl=1, dis=27, kappa=1)
    #         cont_spec = res['cont_ref']
    #         cont_spec_b = res['cont_b']
    #         init_spec.append(cont_spec)
    #         init_spec_b.append(cont_spec_b)

    # metric_total_sum = 0
    # metric_old_total_sum = 0
    metric_b_total_sum = 0
    metric_b_old_total_sum = 0

    for ind in tqdm(range(N_VAL)):
        q = test_signal[ind]
        res = nsev(q, t, M=n_span, Xi1=-xi_span / 2., Xi2=xi_span / 2. - dxi, cst=1, bsl=1, dis=27, kappa=1)
        # cont_spec = res['cont_ref']
        cont_spec_b = res['cont_b']
        # metric_total_sum += get_metric(np.real(init_spec[ind]), np.imag(init_spec[ind]), np.real(cont_spec), np.imag(cont_spec))
        # metric_old_total_sum += get_metric_old(np.real(init_spec[ind]), np.imag(init_spec[ind]), np.real(cont_spec), np.imag(cont_spec))

        # metric_total_sum += get_metric(np.real(test_spec[ind]), np.imag(test_spec[ind]), np.real(cont_spec),
        #                                np.imag(cont_spec))
        # metric_old_total_sum += get_metric_old(np.real(test_spec[ind]), np.imag(test_spec[ind]), np.real(cont_spec),
        #                                        np.imag(cont_spec))

        metric_b_total_sum += get_metric(np.real(test_spec[ind]), np.imag(test_spec[ind]), np.real(cont_spec_b),
                                       np.imag(cont_spec_b))
        metric_b_old_total_sum += get_metric_old(np.real(test_spec[ind]), np.imag(test_spec[ind]), np.real(cont_spec_b),
                                               np.imag(cont_spec_b))

    # metric_values.append(metric_total_sum / N_VAL)
    # metric_values_old.append(metric_old_total_sum / N_VAL)
    metric_values.append(metric_b_total_sum / N_VAL)
    metric_values_old.append(metric_b_old_total_sum / N_VAL)
    print("Metric value:", metric_values[-1])
    print("Metric old value:", metric_values_old[-1])

    toc = timeit.default_timer()
    print("SNR =", noise_level, ":", toc - tic, "s")  # elapsed time in seconds

print("Metric:", metric_values)
print("Metric old:", metric_values_old)

# For r
# For 300
# For real NFT
# Metric: [0.0, 2.7761024839837654, 2.156770134492216, 1.8205077799976008, 1.7023482402691745, 1.5908809668948591, 1.5344094554024976, 1.4757994214209347, 1.4418762930845392]
# Metric old: [0.0, 1.8207327814216474, 1.5836177119998787, 1.4533501765939127, 1.499796710829366, 1.6196232928072516, 1.492093109456648, 1.4685252929165318, 1.3958988781358481]
# For cut NFT (which we have)
# Metric: [3.7581061469232076e-06, 2.000223605356401, 1.1633291355477435, 0.6775291711576132, 0.4889607397946903, 0.31041902537582106, 0.22126939592044736, 0.12264113986035656, 0.06911662218258731]
# Metric old: [3.765588210533114e-11, 1.287330612895487, 0.8069768800312648, 0.494667841826279, 0.5084175841803099, 0.42208392062054995, 0.23234164347943395, 0.035302829618211275, 0.02154640633867798]


# For 1000
# For real NFT
# Metric: [0.0, 2.7757025187452533, 2.1652351230315117, 1.8255817001928962, 1.7064385625754928, 1.597269387877736, 1.5344354298690155, 1.4740753141925274, 1.442307001993015]
# Metric old: [0.0, 2.088469068237767, 1.7713172817401663, 1.66310869580991, 2.0120725265023722, 1.8873380353587388, 1.6786045413560595, 1.5091479714858316, 1.5202086686142704]
# For cut NFT (which we have)
# Metric: [3.793058908099825e-06, 2.002332177860531, 1.171196001039834, 0.6789501530757457, 0.4890379951970322, 0.3165680432729659, 0.22118579376838818, 0.12421059746731389, 0.07046296976647183]
# Metric old: [2.1087058996300056e-10, 1.5777430960701642, 0.9841811111770824, 0.7090715728075828, 0.728038144306568, 0.6804689496707429, 0.40086166326798583, 0.17020646357211333, 0.09078222236264617]

# For b
# For 100
# Metric: [5.394700125541834e-06, 3.5994090020065297, 2.0400358564681396, 1.1503027249697058, 0.8141474746201331, 0.5142705992765803, 0.3635929866584285, 0.20517536320555682, 0.11474458947021147]
# Metric old: [8.155705179624958e-14, 0.034960165728637936, 0.01129315434675501, 0.003601876314506879, 0.0018040104952160305, 0.0007191256723182832, 0.0003594305729029271, 0.00011446982091149832, 3.579104640654505e-05]

# For 1000
# Metric: [5.354893447854767e-06, 3.5925127448040963, 2.0379069492690434, 1.1496314868106166, 0.8138458213037458, 0.5149344691473067, 0.3642366028519659, 0.20484367750633975, 0.11511143560191546]
# Metric old: [8.041789634300077e-14, 0.03491118245679198, 0.01128650449296032, 0.0035983187113718274, 0.0018033545107849567, 0.0007217725767606908, 0.00036115688905531066, 0.00011427734176220404, 3.605462703567289e-05]

# import matplotlib.pyplot as plt
#
# fig, axs = plt.subplots(3, 1)
# axs[0].plot(t, np.absolute(q))
# axs[0].set_xlim(-t_span / 2, t_span / 2)
# axs[0].set_xlabel('Time')
# axs[0].set_ylabel('Power')
# axs[0].grid(True)
#
# axs[1].plot(xi, abs(cont_spec - test_spec[ind]))
# # axs[1].set_xlim(-np.pi / t_span * nt / (2 * np.pi), np.pi / t_span * nt / (2 * np.pi))
# axs[1].set_xlim(-xi_span / 2, xi_span / 2)
# axs[1].set_xlabel('Xi')
# axs[1].set_ylabel('Spectral Power')
# axs[1].set_yscale('log')
# axs[1].grid(True)
#
# axs[2].plot(abs(np.fft.fftshift(np.fft.fft(q))))
# axs[2].set_xlim(490, 540)
# axs[2].set_xlabel('Freq')
# axs[2].set_ylabel('Power')
# axs[2].grid(True)
#
#
# fig.show()
