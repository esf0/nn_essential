# import os
import numpy as np
# import scipy as sp
# import tensorflow as tf
# import commpy
import pandas as pd

# import matplotlib
# import matplotlib.pyplot as plt

# import signal_generation as sg
# import waveform_optimiser as wf
# import channel_model as ch
import hpcom

# data_dir = "/work/ec180/ec180/esedov/data_collection/"
# data_dir = "/home/esf0/PycharmProjects/nn_essential/benchmark/data/"
data_dir = 'C:/Users/190243539/PycharmProjects/nn_essential/benchmark/data/'
# job_name = 'transocean_wo_noise_1'
# job_name = 'errorstat_wo_noise_1'
# job_name = 'benchmark_w_noise_1'
# job_name = 'extended_w_noise_5'
job_name = 'mid_range_wwo_noise_5'
# job_name = 'new_w_noise_1'
# job_name = 'test'

df = pd.DataFrame()

n_channels_list = [1]
# n_channels_list = [1, 3, 7]
# n_channels_list = [1, 7, 15]
# p_ave_dbm_list = np.arange(61) * 0.5 - 20
# p_ave_dbm_list = np.arange(31) * 1 - 20
# p_ave_dbm_list = [0]
# p_ave_dbm_list = [1, 2, 3, 4, 5, 6, 7, 8]
# p_ave_dbm_list = [0, 1, 2, 3, 4, 5, 6, 7, 8]
# p_ave_dbm_list = [0, 1, 2, 3, 4, 5, 6, 7]
# p_ave_dbm_list = [-1, -2, -3, -4, -5, 0, 1, 2, 3, 4, 5, 6]
# p_ave_dbm_list = [0, -1, -2, -3, -4, -5, -6, 7, 8, 9, 10]
# p_ave_dbm_list = [-10, -2, -3, -4, -5, 0, 1, 2, 3, 4, 5, 6]

p_ave_dbm_list = [3, 4, 5, 6, 7, 8, 9, 10]

# n_span_list = [6]
# n_span_list = [15]
# n_span_list = np.arange(1, 26)
n_span_list = [6, 8, 10, 12, 14]  # 480, 640, 800, 960, 1120
# n_span_list = [10, 15, 20]
# n_span_list = [12, 13, 14, 15, 16, 17, 18, 19, 20]
# n_span_list = [38, 50, 75]
# n_span_list = np.arange(1, 21)
# n_runs = 1
n_runs = 16
# n_runs = 256

noise_figure_db_list = [-200, 4.5]
# noise_figure_db = 4.5
# noise_figure_db = -200
z_span = 80

print(p_ave_dbm_list)
print(n_span_list)

for noise_figure_db in noise_figure_db_list:
    for n_channels in n_channels_list:
        for p_ave_dbm in p_ave_dbm_list:
            for n_span in n_span_list:
                for run in range(n_runs):
                    print(f'run = {run} / n_channels = {n_channels} / p_dbm = {p_ave_dbm} / n_span = {n_span} / noise = {noise_figure_db}')
                    wdm_full = hpcom.signal.create_wdm_parameters(n_channels=n_channels, p_ave_dbm=p_ave_dbm, n_symbols=2 ** 16, m_order=16, roll_off=0.1, upsampling=16,
                                                   downsampling_rate=1, symb_freq=34e9, channel_spacing=75e9, n_polarisations=2, seed='time')

                    channel_full = hpcom.channel.create_channel_parameters(n_spans=n_span,
                                                           z_span=z_span,
                                                           alpha_db=0.2,
                                                           gamma=1.2,
                                                           noise_figure_db=noise_figure_db,
                                                           dispersion_parameter=16.8,
                                                           dz=1)

                    result_channel = hpcom.channel.full_line_model_wdm(channel_full, wdm_full, channels_type='middle')

                    result_dict = {}

                    # result_dict['wdm'] = wdm_full
                    # result_dict['channel'] = channel_full

                    result_dict['run'] = run
                    result_dict['n_channels'] = n_channels
                    result_dict['p_ave_dbm'] = p_ave_dbm
                    result_dict['z_km'] = n_span * 80

                    result_dict['noise_figure_db'] = channel_full['noise_figure_db']
                    result_dict['gamma'] = channel_full['gamma']
                    result_dict['z_span'] = channel_full['z_span']
                    result_dict['dispersion_parameter'] = channel_full['dispersion_parameter']
                    result_dict['dz'] = channel_full['dz']

                    result_dict['points_x_orig'] = result_channel['points_x_orig']
                    result_dict['points_x'] = result_channel['points_x']
                    result_dict['points_x_shifted'] = result_channel['points_x_shifted']

                    result_dict['points_y_orig'] = result_channel['points_y_orig']
                    result_dict['points_y'] = result_channel['points_y']
                    result_dict['points_y_shifted'] = result_channel['points_y_shifted']

                    result_dict['ber_x'] = result_channel['ber_x']
                    result_dict['ber_y'] = result_channel['ber_y']

                    result_dict['q_x'] = result_channel['q_x']
                    result_dict['q_y'] = result_channel['q_y']

                    df = df.append(result_dict, ignore_index=True)

        df.to_pickle(data_dir + 'data_collected_' + job_name + '.pkl')

