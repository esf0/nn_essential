# import os
import numpy as np
# import scipy as sp
# import tensorflow as tf
# import commpy
import pandas as pd

# import matplotlib
# import matplotlib.pyplot as plt

# import signal_generation as sg
# import waveform_optimiser as wf
import channel_model as ch

data_dir = "/work/ec180/ec180/esedov/data_benchmark/"
job_name = 'test'

df = pd.DataFrame()

# n_channels_list = [1, 7, 15]
n_channels_list = [7]
# p_ave_dbm_list = np.arange(61) * 0.5 - 20
# p_ave_dbm_list = np.arange(31) * 1 - 20
p_ave_dbm_list = [4]
# n_span_list = [6, 9, 12, 15, 18, 20, 21, 24, 25]
n_span_list = [12]

for n_channels in n_channels_list:
    for p_ave_dbm in p_ave_dbm_list:
        for n_span in n_span_list:
            print(n_channels, p_ave_dbm, n_span)
            wdm_full = ch.create_wdm_parameters(n_channels=n_channels, p_ave_dbm=p_ave_dbm, n_symbols=2 ** 16, m_order=16, roll_off=0.1, upsampling=16,
                                           downsampling_rate=1, symb_freq=34e9, channel_spacing=75e9, n_polarisations=2)

            channel_full = ch.create_channel_parameters(n_spans=n_span,
                                                   z_span=80,
                                                   alpha_db=0.2,
                                                   gamma=1.2,
                                                   noise_figure_db=4.5,
                                                   dispersion_parameter=16.8,
                                                   dz=1)

            result_channel = ch.full_line_model_wdm(channel_full, wdm_full)

            result_dict = {}

            result_dict['wdm'] = wdm_full
            result_dict['channel'] = channel_full

            result_dict['n_channels'] = n_channels
            result_dict['p_ave_dbm'] = p_ave_dbm
            result_dict['z_km'] = n_span * 80

            result_dict['ber_x'] = result_channel['ber_x']
            result_dict['ber_y'] = result_channel['ber_y']

            result_dict['q_x'] = result_channel['q_x']
            result_dict['q_y'] = result_channel['q_y']

            df = df.append(result_dict, ignore_index=True)

    df.to_pickle(data_dir + 'benchmark_' + job_name + '.pkl')

