import numpy as np
import pickle
import sys
from scipy.optimize import minimize, NonlinearConstraint
from scipy.fftpack import fft, ifft, fftfreq, fftshift, ifftshift
from ssfm import fiber_propogate, get_soliton_pulse, get_gauss_pulse
import matplotlib.pyplot as plt
import signal_generation as sg


def cut_spectrum(spectrum, freq, bandwidth):
    if len(freq) != len(spectrum):
        print("Error: spectrum and frequency arrays have different length")
        return -1

    spec_rest = np.zeros(len(freq), dtype=np.complex128)
    spec_cut = np.zeros(len(freq), dtype=np.complex128)
    for i in range(len(freq)):
        if abs(freq[i]) <= bandwidth / 2:
            spec_rest[i] = spectrum[i]
        else:
            spec_cut[i] = spectrum[i]

    return spec_rest, spec_cut


def get_energy(x_c, t_span=32.0):
    return t_span * np.mean(np.power(np.absolute(x_c), 2))


def get_soliton_sinc(t, bandwidth):
    soliton_pulse = get_soliton_pulse(t, 0.5, 1, beta2, gamma)
    spec_rest, spec_cut = cut_spectrum(fftshift(fft(soliton_pulse)), w, bandwidth)
    return ifft(ifftshift(spec_rest))


def get_soliton_sinc(t, w, bandwidth, beta2, gamma):
    soliton_pulse = get_soliton_pulse(t, 0.5, 1, beta2, gamma)
    spec_rest, spec_cut = cut_spectrum(fftshift(fft(soliton_pulse)), w, bandwidth)
    return ifft(ifftshift(spec_rest))


def compose(x_c):
    # x_c -> x_r
    x_r = np.zeros(len(x_c) * 2)
    for i in range(len(x_c)):
        x_r[2 * i] = np.real(x_c[i])
        x_r[2 * i + 1] = np.imag(x_c[i])
    return x_r


def decompose(x_r):
    # x_r -> x_c
    x_c = np.zeros(len(x_r) // 2, dtype=np.complex128)
    for i in range(len(x_c)):
        x_c[i] = x_r[2 * i] + 1.0j * x_r[2 * i + 1]
    return x_c


def get_outband_energy(spec):
    spec_rest, spec_cut = cut_spectrum(spec, w, band_limit)
    # get_energy(ifft(ifftshift(spec_rest)))
    return get_energy(ifft(ifftshift(spec_cut))), get_energy(ifft(ifftshift(spec_rest)))


def get_outband_energy(spec, t_span, w, band_limit):
    spec_rest, spec_cut = cut_spectrum(spec, w, band_limit)
    return get_energy(ifft(ifftshift(spec_cut)), t_span), get_energy(ifft(ifftshift(spec_rest)), t_span)


def energy_constraint(x_r):
    return get_energy(ifft(ifftshift(add_zeros_to_spectrum(decompose(x_r)))))
    # return get_energy(decompose(x_r))


def band_limit_constraint(x_r):
    spec = fftshift(fft(decompose(x_r)))
    return get_outband_energy(spec)


def get_spec_limited(spectrum):
    return np.array([spectrum[i] for i in range(len(w)) if abs(w[i]) <= band_limit / 2])


def get_spec_limited(spectrum, w, band_limit):
    return np.array([spectrum[i] for i in range(len(w)) if abs(w[i]) <= band_limit / 2])


def add_zeros_to_spectrum(w_c):
    full_spectrum = np.zeros(len(w), dtype=np.complex128)
    i_init = -1
    for i in range(len(w)):
        if abs(w[i]) <= band_limit / 2:
            if i_init == -1:
                i_init = i
            full_spectrum[i] = w_c[i - i_init]

    return full_spectrum


def add_zeros_to_spectrum(w_c, w, band_limit):
    full_spectrum = np.zeros(len(w), dtype=np.complex128)
    i_init = -1
    for i in range(len(w)):
        if abs(w[i]) <= band_limit / 2:
            if i_init == -1:
                i_init = i
            full_spectrum[i] = w_c[i - i_init]

    return full_spectrum


def f_minimise(x_r):
    # Minimize E_lost after propagation at fiber

    # x_c -> x_r
    # x_c = decompose(x_r)
    w_c = decompose(x_r)

    # for w-space
    w_c = add_zeros_to_spectrum(w_c)
    x_c = ifft(ifftshift(w_c))

    # fiber propogation
    signal_end = fiber_propogate(x_c, t_span, z_prop, nz_span, gamma=gamma, beta2=beta2)
    spectrum = fftshift(fft(signal_end))
    e_lost, e_rest = get_outband_energy(spectrum)
    # print(e_lost, e_rest)

    return e_lost


def get_signal_from_cut(w_init, band_limit, t_span, nt_span, z_prop, nz_span, gamma, beta2, key=0):
    # Return full spectrum and signal

    w = np.array([(i - nt_span / 2) * (2. * np.pi / nt_span) for i in range(nt_span)])

    # for w-space
    w_init = add_zeros_to_spectrum(w_init, w, band_limit)
    x_c = ifft(ifftshift(w_init))

    # fiber propogation
    signal_end = fiber_propogate(x_c, t_span, z_prop, nz_span, gamma=gamma, beta2=beta2)
    spectrum = fftshift(fft(signal_end))

    if key == 0:
        return w_init, x_c, spectrum, signal_end
    elif key == 1:
        return w_init, x_c
    elif key == 2:
        return spectrum, signal_end
    else:
        return 0


def get_e_lost(w_init, band_limit, t_span, nt_span, z_prop, nz_span, gamma, beta2):
    # Find E_lost after propagation at fiber

    w = np.array([(i - nt_span / 2) * (2. * np.pi / nt_span) for i in range(nt_span)])
    spectrum, signal_end = get_signal_from_cut(w_init, band_limit, t_span, nt_span, z_prop, nz_span, gamma, beta2, key=2)
    e_lost, e_rest = get_outband_energy(spectrum, t_span, w, band_limit)
    # print(e_lost, e_rest)

    return e_lost




if __name__ == "__main__":

    # z_prop, energy, band_limit

    energy_sys = float(sys.argv[1])

    for band_limit in [0.5, 0.4, 0.3, 0.2]:
        for z_prop in [1.0 * i for i in range(1, 21)]:

            nt_span = 2 ** 8
            t_span = 32.0
            beta2 = -1.0
            gamma = 1.0
            # z_prop = 10.0
            nz_span = 2 ** 12

            energy_lb = energy_sys
            energy_ub = energy_sys + 0.1
            # band_limit = 0.5

            dt = t_span / nt_span
            t = np.array([(i - nt_span / 2) * dt for i in range(nt_span)])
            w = np.array([(i - nt_span / 2) * (2. * np.pi / nt_span) for i in range(nt_span)])

            nlc_energy = NonlinearConstraint(energy_constraint, energy_lb, energy_ub)
            # nlc_band_limit = NonlinearConstraint(band_limit_constraint, 0.0, 0.0)

            signal_init = sg.set_energy(get_soliton_sinc(t, band_limit), energy_lb, dt)
            x_init = compose(get_spec_limited(fftshift(fft(signal_init))))

            res = minimize(f_minimise, x0=x_init, constraints=[nlc_energy], options={'maxiter': 1000, 'disp': True})
            print(res)

            # write result to file
            with open('wf_opt_results/wf_opt_result_' + str(energy_sys) +
                      '_' + str(band_limit) + '_' + str(z_prop) + '.pkl', 'wb') as output:
                pickle.dump(res, output, pickle.HIGHEST_PROTOCOL)

            # to read
            # with open('wf_opt_results/test_res.pkl', 'rb') as input:
            #     res_load = pickle.load(input)

            # print(res_load)

            # solution = ifft(ifftshift(add_zeros_to_spectrum(decompose(res['x']))))
            # sol_init = ifft(ifftshift(add_zeros_to_spectrum(decompose(x_init))))
            #
            # print("Optimised E_rad:", f_minimise(res['x']))
            # print("Init E_rad:", f_minimise(x_init))
            #
            # fig, axs = plt.subplots(3, 1)
            # fig.set_figwidth(10)
            # fig.set_figheight(15)
            # axs[0].plot(t, np.absolute(solution), 'blue')
            # axs[0].plot(t, np.absolute(sol_init), 'red')
            # axs[0].set_xlim(-t_span / 2, t_span / 2)
            # axs[0].set_xlabel('T')
            # axs[0].set_ylabel('Power')
            # axs[0].grid(True)
            #
            # axs[1].plot(w, np.absolute(fftshift(fft(solution))), 'blue')
            # axs[1].plot(w, np.absolute(fftshift(fft(sol_init))), 'red')
            # axs[1].set_xlim(-2, 2)
            # axs[1].set_xlabel('Normalized Frequency')
            # axs[1].set_ylabel('Spectral Power')
            # axs[1].grid(True)
            #
            # axs[2].plot(t, np.absolute(solution - sol_init), 'blue')
            # axs[2].set_xlim(-t_span / 2, t_span / 2)
            # axs[2].set_xlabel('T')
            # axs[2].set_ylabel('Abs diff')
            # axs[2].grid(True)
            #
            # fig.show()
            #
            # fig2, axs2 = plt.subplots(4, 1)
            # fig2.set_figwidth(10)
            # fig2.set_figheight(20)
            # axs2[0].plot(t, np.real(solution), 'blue')
            # axs2[0].plot(t, np.real(sol_init), 'red')
            # axs2[0].set_xlim(-t_span / 2, t_span / 2)
            # axs2[0].set_xlabel('T')
            # axs2[0].set_ylabel('Real')
            # axs2[0].grid(True)
            #
            # axs2[1].plot(t, np.imag(solution), 'blue')
            # axs2[1].plot(t, np.imag(sol_init), 'red')
            # axs2[1].set_xlim(-t_span / 2, t_span / 2)
            # axs2[1].set_xlabel('T')
            # axs2[1].set_ylabel('Imag')
            # axs2[1].grid(True)
            #
            # axs2[2].plot(w, np.real(fftshift(fft(solution))), 'blue')
            # axs2[2].plot(w, np.real(fftshift(fft(sol_init))), 'red')
            # axs2[2].set_xlim(-band_limit / 2 - 0.1, band_limit / 2 + 0.1)
            # axs2[2].set_xlabel('Normalized Frequency')
            # axs2[2].set_ylabel('Real spec')
            # axs2[2].grid(True)
            #
            # axs2[3].plot(w, np.imag(fftshift(fft(solution))), 'blue')
            # axs2[3].plot(w, np.imag(fftshift(fft(sol_init))), 'red')
            # axs2[3].set_xlim(-band_limit / 2 - 0.1, band_limit / 2 + 0.1)
            # axs2[3].set_xlabel('Normalized Frequency')
            # axs2[3].set_ylabel('Imag spec')
            # axs2[3].grid(True)
            #
            # fig2.show()