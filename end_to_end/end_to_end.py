import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Dense, Input, Flatten, Conv2D, MaxPool2D
from keras.layers import Input, Dense, Reshape, Permute
from tensorflow.keras.models import Model

import numpy as np
import random

import hpcom


def all_combinations_of_n_bits(n):
    combinations = []
    for i in range(2**n):
        combination = [int(x) for x in bin(i)[2:].zfill(n)]
        combinations.append(combination)
    return combinations


# Channel layer
def channel_test(inputs, channel_parameters, wdm, verbose):
    # Channel model

    # split points for x and y polarisations
    points_x = inputs[:len(inputs) // 2]
    points_y = inputs[len(inputs) // 2:]
    points_x = points_x[:, 0] + 1j * points_x[:, 1]  # convert two float to complex
    points_y = points_y[:, 0] + 1j * points_y[:, 1]
    # print(np.shape(points_x), np.shape(points_y))

    # Set average signal power

    result = hpcom.channel.full_line_model_wo_metrics(channel_parameters, wdm, points=([points_x], [points_y]), verbose=verbose)
    # [] because we can have several channels [p_x_0, p_x_1, ..]

    # print(np.shape(result['points_x_shifted']), np.shape(result['points_y_shifted']))
    points_x_shifted = result['points_x_shifted']
    points_y_shifted = result['points_y_shifted']
    # print(np.shape(points_x_shifted), np.shape(points_y_shifted))

    points_x_shifted = np.stack([points_x_shifted.real, points_x_shifted.imag], -1)
    points_y_shifted = np.stack([points_y_shifted.real, points_y_shifted.imag], -1)

    output = np.concat([points_x_shifted, points_y_shifted], 0)

    # points_x_shifted = tf.convert_to_tensor(points_x_shifted)
    # points_y_shifted = tf.convert_to_tensor(points_y_shifted)
    #
    # output = tf.concat([points_x_shifted, points_y_shifted], 0)
    return output


def get_scale_constellation_for_nn(pilot_points, p_ave):

    pilot_points = pilot_points[:, 0] + 1j * pilot_points[:, 1]  # convert two float to complex
    # if we have two polarisations then we have to divide average power by 2
    scale_constellation = np.sqrt(p_ave) / hpcom.modulation.get_sq_of_average_power(pilot_points)
    return scale_constellation


#%%
class EndToEnd(keras.layers.Layer):
    def __init__(self, n_bits, wdm, channel_parameters, verbose=0):
        super().__init__()

        self.coder_dense1 = Dense(128, activation='relu')
        self.coder_dense2 = Dense(128, activation='relu')
        self.coder_dense3 = Dense(2)

        self.decoder_dense1 = Dense(2, activation='relu')
        self.decoder_dense2 = Dense(128, activation='relu')
        self.decoder_dense3 = Dense(128, activation='relu')
        self.decoder_dense4 = Dense(n_bits, activation='sigmoid')

        self.p_ave = wdm['p_ave'] / wdm['n_polarisations']
        self.n_bits = n_bits
        self.combinations = tf.convert_to_tensor(np.array(all_combinations_of_n_bits(n_bits)).reshape((2 ** n_bits, n_bits)), dtype=tf.float32)

        self.wdm = wdm
        self.channel_parameters = channel_parameters
        self.verbose = verbose


    # Channel layer
    def channel(self, inputs):
        # Channel model

        # split points for x and y polarisations
        points_x = inputs[:len(inputs) // 2].numpy()
        points_y = inputs[len(inputs) // 2:].numpy()
        points_x = points_x[:, 0] + 1j * points_x[:, 1]  # convert two float to complex
        points_y = points_y[:, 0] + 1j * points_y[:, 1]
        # print(np.shape(points_x), np.shape(points_y))

        # Set average signal power

        result = hpcom.channel.full_line_model_wo_metrics(self.channel_parameters, self.wdm, points=([points_x], [points_y]), verbose=self.verbose)
        # [] because we can have several channels [p_x_0, p_x_1, ..]

        # print(np.shape(result['points_x_shifted']), np.shape(result['points_y_shifted']))
        points_x_shifted = result['points_x_shifted']
        points_y_shifted = result['points_y_shifted']
        # print(np.shape(points_x_shifted), np.shape(points_y_shifted))

        points_x_shifted = np.stack([points_x_shifted.real, points_x_shifted.imag], -1)
        points_y_shifted = np.stack([points_y_shifted.real, points_y_shifted.imag], -1)

        points_x_shifted = tf.convert_to_tensor(points_x_shifted)
        points_y_shifted = tf.convert_to_tensor(points_y_shifted)

        output = tf.concat([points_x_shifted, points_y_shifted], 0)
        return output


    def call(self, inputs):

        # NN CODER
        x = self.coder_dense1(inputs)
        x = self.coder_dense2(x)
        x = self.coder_dense3(x)

        # print('Initially it was')
        # print(x)

        pilot_points = self.coder_dense1(self.combinations)
        pilot_points = self.coder_dense2(pilot_points)
        pilot_points = self.coder_dense3(pilot_points)
        # pilot_points = pilot_points.numpy()
        # pilot_points = pilot_points[:, 0] + 1j * pilot_points[:, 1]  # convert two float to complex
        #
        # # if we have two polarisations then we have to divide average power by 2
        # scale_constellation = np.sqrt(self.p_ave) / hpcom.modulation.get_sq_of_average_power(pilot_points)
        # self.scale_constellation = scale_constellation
        self.scale_constellation = keras.backend.eval(tf.numpy_function(get_scale_constellation_for_nn, [pilot_points, self.p_ave], tf.float32))

        print('scale: ', self.scale_constellation, type(self.scale_constellation), tf.shape(self.scale_constellation))

        # scale our output. This is the constellation points, coded with NN coder
        x = tf.math.scalar_mul(self.scale_constellation, x)


        # CHANNEL MODEL
        # print('before channel', tf.shape(x))
        # x = self.channel(x)
        x = tf.numpy_function(channel_test, [x, self.channel_parameters, self.wdm, self.verbose], tf.float32)
        # print('after channel', tf.shape(x))
        # de-scale. it seems that it can be optional
        x = tf.math.scalar_mul(1. / self.scale_constellation, x)

        # NN DECODER
        x = self.decoder_dense1(x)
        x = self.decoder_dense2(x)
        x = self.decoder_dense3(x)
        x = self.decoder_dense4(x)

        return x


def test_end_to_end():
    print('start test_end_to_end')
    n_bits = 4
    x_test = tf.convert_to_tensor(np.array(all_combinations_of_n_bits(n_bits)).reshape((2 ** n_bits, n_bits)),
                                  dtype=tf.float32)
    n_symbols = len(x_test)
    x_test = tf.concat([x_test, x_test], 0)

    wdm = hpcom.signal.create_wdm_parameters(n_channels=1, p_ave_dbm=0, n_symbols=n_symbols, m_order=16, roll_off=0.1,
                                             upsampling=16,
                                             downsampling_rate=1, symb_freq=34e9, channel_spacing=0, n_polarisations=2)

    # SSFM fiber
    channel_parameters = hpcom.channel.create_channel_parameters(n_spans=1, z_span=80, alpha_db=0.0, gamma=0,
                                                                 noise_figure_db=-200, dispersion_parameter=16.8, dz=1)

    y = EndToEnd(n_bits, wdm, channel_parameters, verbose=0)(x_test)
    print(y)
    print('test_end_to_end() is ok')


if __name__ == '__main__':
    # ok it seems that something works

    print('Let\'s go!')

    test_end_to_end()

    # main parameters
    # n_bits = 4
    # n_symbols = 2 ** 11
    #
    # wdm = hpcom.signal.create_wdm_parameters(n_channels=1, p_ave_dbm=10, n_symbols=n_symbols, m_order=16, roll_off=0.1,
    #                                          upsampling=16,
    #                                          downsampling_rate=1, symb_freq=34e9, channel_spacing=0, n_polarisations=2)
    #
    # # SSFM fiber
    # channel_parameters = hpcom.channel.create_channel_parameters(n_spans=12, z_span=80, alpha_db=0.2, gamma=1.2,
    #                                                              noise_figure_db=-200, dispersion_parameter=16.8, dz=1)
    #
    # # generate data
    # bits = np.random.randint(0, 2, (2 * n_symbols, n_bits), int)
    #
    # # tf.config.run_functions_eagerly(True)
    #
    # # create model
    # inputs = Input(shape=(None, 4))
    # outputs = EndToEnd(n_bits, wdm, channel_parameters, verbose=0)(inputs)
    # model = Model(inputs=inputs, outputs=outputs)
    # model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'], run_eagerly=True)
    # # run_eagerly=True needed that for some reason TF disables some functions (for performance reason)
    # # e.g. tf.numpy(). But we need it inside our EndToEnd class

    print('Finished')