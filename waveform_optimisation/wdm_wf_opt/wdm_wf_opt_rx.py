import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import pickle
import os

from deap import base, creator, tools

from scipy.fft import fftshift, ifftshift, ifft, fftfreq
from scipy.optimize import minimize, NonlinearConstraint

# import hpcom
from datetime import datetime

from hpcom.signal import create_wdm_parameters, generate_wdm, generate_wdm_optimise, receiver, receiver_wdm,\
    nonlinear_shift, dbm_to_mw, get_default_wdm_parameters, rrcosfilter, update_wdm_parameters_from_json
from hpcom.modulation import get_modulation_type_from_order, get_scale_coef_constellation, \
    get_nearest_constellation_points_unscaled
from hpcom.metrics import get_ber_by_points, get_ber_by_points_ultimate, get_energy, get_average_power, get_evm_ultimate, get_evm
from hpcom.channel import create_channel_parameters, \
    full_line_model, full_line_model_wdm, transceiver_line_model, receiver_model, \
    update_channel_parameters_from_json

from ssfm_gpu.propagation import propagate_manakov, propagate_manakov_backward, \
    propagate_schrodinger, dispersion_compensation_manakov


from wf_opt_additional import *


wdm_json = 'wdm_parameters.json'
channel_json = 'channel_parameters.json'

split = True
load_flag = False

data_dir = 'data/'
job_name = 'rx_type_1_run_1'
job_name_load = 'rx_type_1_run_1'
n_max_iter = 100

filename = data_dir + job_name + '_states.pkl'  # The file where the states will be stored.
filename_load = data_dir + job_name_load + '_states.pkl'
filename_plot = data_dir + job_name + '_plot.png'

filename_wf_opt = data_dir + "wf_opt_" + job_name + ".npy"
filename_wf_opt_load = data_dir + "wf_opt_" + job_name_load + ".npy"

filename_fitnesses = data_dir + "fitnesses_" + job_name + ".npy"

# DEVICE = '/device:GPU'
DEVICE = '/CPU:0'
GPU_NUM = 0
GPU_MEM_LIM = 2048

# create parameters
# wdm = create_wdm_parameters(n_channels=3, p_ave_dbm=3, n_symbols=2 ** 15, m_order=64, roll_off=0.1, upsampling=16,
#                             downsampling_rate=1, symb_freq=34e9, channel_spacing=75e9, n_polarisations=2)
#
#
# channel = create_channel_parameters(n_spans=12,
#                                     z_span=80,
#                                     alpha_db=0.2,
#                                     gamma=1.2,  # 1.2
#                                     noise_figure_db=-200,
#                                     dispersion_parameter=16.8,
#                                     dz=1)

wdm = update_wdm_parameters_from_json(wdm_json)
channel = update_channel_parameters_from_json(channel_json)

fitnesses = []  # list of fitnesses

# def callback(xk, state=None) -> bool:
#
#     print('Callback called back!')
#
#     f_data_save = True
#
#     if state is None:
#         # save x to the file
#         if f_data_save:
#             np.save(filename_wf_opt, xk)
#         # print(xk)
#         return False
#
#     # only for method=‘trust-constr’
#     print("Number of iterations / function evaluation:", state['nit'], state['nfev'])
#
#     # save x to the file
#     save_freq = 1  # frequency of saving
#     if f_data_save and state['nit'] % save_freq == 0:
#             np.save(filename_wf_opt, xk)
#
#     if state['nfev'] >= 10:
#         return True
#     else:
#         return False


# def callback(intermediate_result):
#
#     # intermediate_result is scipy.optimize.OptimizeResult object
#
#     print('Callback called back!')
#
#     f_data_save = True
#
#     # only for method=‘trust-constr’
#     print("Number of iterations / function evaluation:", intermediate_result['nit'], intermediate_result['nfev'])
#
#     # save x to the file
#     save_freq = 1  # frequency of saving
#     if f_data_save and intermediate_result['nit'] % save_freq == 0:
#             np.save(filename_wf_opt, intermediate_result['x'])
#
#     plot_freq = 1
#     if intermediate_result['nit'] % plot_freq == 0:
#         plot_fitnesses(fitnesses, filename_plot)


def callback_rx(xk):

    print('Callback called back!')

    args_for_f = (channel, wdm, None, None, ft_filter_size, 'evm_x', is_odd, split)

    f_data_save = True
    f_add_fitness = True
    # save x to the file
    if f_data_save:
        if os.path.exists(filename_wf_opt):
            existing_xk = np.load(filename_wf_opt)
            new_xk = np.vstack((existing_xk, xk))
            np.save(filename_wf_opt, new_xk)
        else:
            np.save(filename_wf_opt, xk[np.newaxis, :])  # Save xk as a new row

    if f_add_fitness:
        current_fitness = f_minimise_rx(xk, *args_for_f)
        if os.path.exists(filename_fitnesses):
            existing_fitnesses = np.load(filename_fitnesses)
            new_fitnesses = np.concatenate((existing_fitnesses, np.array([current_fitness])))
            np.save(filename_fitnesses, new_fitnesses)
        else:
            np.save(filename_fitnesses, np.array([current_fitness]))  # Save current_fitness as a new row
        plot_fitnesses(np.load(filename_fitnesses), filename_plot)


def f_minimise_rx(x, *args):

    # x - values for optimisation - cutted spectrum with 2n real values
    # and only left half of the spectrum
    x = np.array(x)

    start_time = datetime.now()

    channel = args[0]
    wdm = args[1]
    points_x = args[2]
    points_y = args[3]
    spectrum_size = args[4]
    return_type = args[5]
    is_odd = args[6]
    split = args[7]

    # decompose to complex numbers and restore full spectrum from half
    if split:
        cut_spectrum = restore_array(decompose(x), is_odd=is_odd)
    else:
        cut_spectrum = decompose(x)

    # for w-space
    spectrum = spectrum_add_zero_sides(cut_spectrum, spectrum_size)
    filter_values = ifft(ifftshift(spectrum))

    n_t = wdm['upsampling'] * wdm['n_symbols']
    filter_upsampled = upsample_zero_sides(filter_values, n_t)
    filter_upsampled = tf.cast(filter_upsampled, tf.complex128)
    filter_upsampled_conj = tf.cast(np.conjugate(filter_upsampled), tf.complex128)

    ft_filter_values = tf.signal.fftshift(tf.signal.fft(filter_upsampled))
    ft_filter_values_conj = tf.signal.fftshift(tf.signal.fft(filter_upsampled_conj))

    norm_coef = tf.reduce_mean(tf.math.pow(tf.math.abs(ft_filter_values), 2))
    print("Normalisation coefficient of ft_filter_values", norm_coef)

    ft_filter_values = ft_filter_values * tf.cast(tf.math.sqrt(wdm['upsampling'] / norm_coef), tf.complex128)

    # result = ch.full_line_model_optimise(channel, wdm, points_x, points_y, ft_filter_values, ft_filter_values_conj, return_type=return_type)

    ft_filter_tx = [[ft_filter_values for _ in range(wdm['n_channels'])] for _ in range(2)]
    ft_filter_rx = [[ft_filter_values_conj for _ in range(wdm['n_channels'])] for _ in range(2)]

    # result = full_line_model_wdm(channel, wdm, verbose=2, optimise=return_type, channels_type='middle',
    #                              ft_filter_values_tx=ft_filter_tx,
    #                              ft_filter_values_rx=ft_filter_rx)

    result = receiver_model(wdm,
                            result_tx['signal'],
                            result_tx['points_orig'],
                            ft_filter_rx,
                            optimise=return_type,
                            channels_type='middle', verbose=2)

    end_time = datetime.now()
    time_diff = (end_time - start_time)
    execution_time = time_diff.total_seconds() * 1000
    print("Function evaluation took", execution_time, "ms")

    return result


# if DEVICE == '/device:GPU':
# allocate GPU memory
gpus = tf.config.list_physical_devices('GPU')
if gpus:
    # Restrict TensorFlow to only allocate 4GB of memory on the first GPU
    try:
        tf.config.set_logical_device_configuration(
            gpus[GPU_NUM],
            [tf.config.LogicalDeviceConfiguration(memory_limit=GPU_MEM_LIM)])
        logical_gpus = tf.config.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Virtual devices must be set before GPUs have been initialized
        print(e)


try:
    # Specify an invalid GPU device
    if DEVICE == '/device:GPU':
        device_name = '/device:GPU:' + str(GPU_NUM)
    else:
        device_name = DEVICE

    with tf.device(device_name):

        # calculate transmission once
        result_tx = transceiver_line_model(channel, wdm, verbose=3)

        # create optimisation parameters
        ft_filter_size = 2 ** 10  # size of optimized spectrum
        bandwidth_frac = 1.1

        init_filter_to_optimize = rrcosfilter(ft_filter_size, wdm['roll_off'], 1. / wdm['symb_freq'], wdm['sample_freq'])
        ft_init_filter = tf.signal.fftshift(tf.signal.fft(init_filter_to_optimize))
        freq = fftshift(fftfreq(ft_filter_size, d=1. / wdm['sample_freq']))
        init_cut_spectrum = get_spec_limited(ft_init_filter.numpy(), freq, wdm['symb_freq'] * bandwidth_frac)
        freq_cut = get_spec_limited(freq, freq, wdm['symb_freq'] * bandwidth_frac)
        print('Efficient number of points to optimise:', len(init_cut_spectrum))


        if load_flag:
            x0 = np.load(filename_wf_opt_load, allow_pickle=True)
        else:
            # set initial approximation
            if len(init_cut_spectrum) % 2 == 0:
                is_odd = False
            else:
                is_odd = True

            if split:
                x0 = compose(split_array(init_cut_spectrum))
            else:
                x0 = compose(init_cut_spectrum)

        n_point_x = len(x0)

        # print(type(x0), type(init_cut_spectrum), np.shape(x0), np.shape(init_cut_spectrum))
        print('Check x0 mean ->', np.mean(x0), 'with len', n_point_x)  # I don't why but we have to do smt with x0 or it will be nan


        # main part
        args = (channel, wdm, None, None, ft_filter_size, 'evm_x', is_odd, split)

        # optimise
        res = minimize(f_minimise_rx,
                       method='BFGS',
                       x0=x0,
                       args=args,
                       callback=callback_rx,
                       options={'maxiter': n_max_iter, 'disp': True})

        # write result to file
        with open(filename, 'wb') as output:
            pickle.dump(res, output, pickle.HIGHEST_PROTOCOL)


except RuntimeError as e:
    print(e)