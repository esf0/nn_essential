import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import pickle
import os

def compose(x_c):
    # x_c -> x_r
    # make a real valued array of size 2n
    n = len(x_c)
    x_r = np.zeros(2 * n)
    x_r[:n] = np.real(x_c)
    x_r[n:] = np.imag(x_c)
    return x_r


def decompose(x_r):
    # x_r -> x_c
    n = len(x_r) // 2
    x_c = np.zeros(n, dtype=np.complex128)
    x_c = x_r[:n] + 1j * x_r[n:]
    return x_c


def upsample_zero_sides(f, new_size):
    n = len(f)
    zeros = np.zeros(int((new_size - n) / 2), dtype=np.complex128)
    return np.concatenate((zeros, f, zeros))


def spectrum_add_zero_sides(spectrum, new_size):
    n = len(spectrum)
    n_add = int((new_size - n) / 2)
    if n % 2 == 0:
        zeros_left = np.zeros(n_add, dtype=np.complex128)
        zeros_right = np.zeros(n_add, dtype=np.complex128)
    else:
        zeros_left = np.zeros(n_add + 1, dtype=np.complex128)
        zeros_right = np.zeros(n_add, dtype=np.complex128)
    # print(np.shape(zeros_left), np.shape(spectrum), np.shape(zeros_right))
    return np.concatenate((zeros_left, spectrum, zeros_right))


def get_spec_limited(spectrum, freq, bandwidth):
    ind = np.where(np.logical_and(freq >= -bandwidth / 2, freq <= bandwidth / 2))
    return spectrum[ind]


def split_array(arr):
    """
    Splits the input array in half, returning the right part.
    In case of odd length, the central element is included in the result.
    """
    if len(arr) % 2 == 0:
        midpoint = len(arr) // 2
    else:
        midpoint = len(arr) // 2 + 1
    return arr[:midpoint]


def restore_array(half, is_odd=False):
    """
    Restores the input array to its original form by mirroring its elements.
    'is_odd' flag indicates whether the original array length was odd.
    """
    if is_odd:
        # If the original array length was odd, remove the central element before mirroring
        return np.concatenate((half[:-1], half[::-1]))
    else:
        # If it was even, just mirror the elements
        return np.concatenate((half, half[::-1]))


# def save_state(dir, pop, best_individual, avg_fitnesses, generation):
#     state = {"pop": pop, "best_individual": best_individual,
#              "avg_fitnesses": avg_fitnesses, "generation": generation}
#     with open(dir + f'state_gen_{generation}.pkl', 'wb') as f:
#         pickle.dump(state, f)
#
#


def save_state(filename, pop, best_individual, avg_fitnesses, generation):
    # The file where the states will be stored.
    # filename = 'states.pkl'

    # Load the states if the file exists, otherwise start with an empty list.
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            states = pickle.load(f)
    else:
        states = []

    # Append the new state.
    state = {"pop": pop, "best_individual": best_individual,
             "avg_fitnesses": avg_fitnesses, "generation": generation}
    states.append(state)

    # Write all the states back into the file.
    with open(filename, 'wb') as f:
        pickle.dump(states, f)


def plot_and_save(avg_fitnesses, best_fitnesses, filename):
    # Update font size
    plt.rcParams.update({'font.size': 14})

    plt.figure(figsize=(12, 8))

    # Add markers to your plot ('o' stands for 'circle marker')
    plt.plot(avg_fitnesses, 'o-', label='Average Fitness')
    plt.plot(best_fitnesses, 'o-', label='Best Fitness')

    plt.xlabel('Generation')
    plt.ylabel('Fitness')

    plt.yscale('log')  # set log scale for y-axis

    # Increase number of y-axis ticks
    y_locator = ticker.LogLocator(base=10.0, numticks=15)
    plt.gca().yaxis.set_major_locator(y_locator)

    plt.legend()

    # Show grid
    plt.grid(True, which="both", ls="--", linewidth=0.5)

    plt.savefig(filename)
    plt.close()


def plot_fitnesses(fitness, filename):
    # Update font size
    plt.rcParams.update({'font.size': 20})

    plt.figure(figsize=(20, 10))

    # Add markers to your plot ('o' stands for 'circle marker')
    plt.plot(fitness, 'o-', label='Fitness')

    plt.xlabel('Iteration')
    plt.ylabel('Fitness')

    plt.yscale('log')  # set log scale for y-axis

    # Increase number of y-axis ticks
    y_locator = ticker.LogLocator(base=10.0, numticks=15)
    plt.gca().yaxis.set_major_locator(y_locator)

    plt.legend()

    # Show grid
    plt.grid(True, which="both", ls="--", linewidth=0.5)

    plt.savefig(filename)
    plt.close()