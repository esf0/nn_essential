import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import pickle
import os

from deap import base, creator, tools

from scipy.fft import fftshift, ifftshift, ifft, fftfreq
from scipy.optimize import minimize, NonlinearConstraint

# import hpcom
from datetime import datetime

from hpcom.signal import create_wdm_parameters, generate_wdm, generate_wdm_optimise, receiver, receiver_wdm,\
    nonlinear_shift, dbm_to_mw, get_default_wdm_parameters, rrcosfilter, update_wdm_parameters_from_json
from hpcom.modulation import get_modulation_type_from_order, get_scale_coef_constellation, \
    get_nearest_constellation_points_unscaled
from hpcom.metrics import get_ber_by_points, get_ber_by_points_ultimate, get_energy, get_average_power, get_evm_ultimate, get_evm
from hpcom.channel import create_channel_parameters, full_line_model, full_line_model_wdm, update_channel_parameters_from_json

from ssfm_gpu.propagation import propagate_manakov, propagate_manakov_backward, \
    propagate_schrodinger, dispersion_compensation_manakov


from wf_opt_additional import *


wdm_json = 'wdm_parameters.json'
channel_json = 'channel_parameters.json'

split = True
load_flag = True

data_dir = 'data/'
job_name = 'genetic_type_1_run_3'
job_name_load = 'genetic_type_1_run_2'
# n_max_iter = 1

filename = data_dir + job_name + '_states.pkl'  # The file where the states will be stored.
filename_load = data_dir + job_name_load + '_states.pkl'
filename_plot = data_dir + job_name + '_plot.png'


GPU_NUM = 0
GPU_MEM_LIM = 5000

# create parameters
# wdm = create_wdm_parameters(n_channels=3, p_ave_dbm=3, n_symbols=2 ** 15, m_order=64, roll_off=0.1, upsampling=16,
#                             downsampling_rate=1, symb_freq=34e9, channel_spacing=75e9, n_polarisations=2)
#
#
# channel = create_channel_parameters(n_spans=12,
#                                     z_span=80,
#                                     alpha_db=0.2,
#                                     gamma=1.2,  # 1.2
#                                     noise_figure_db=-200,
#                                     dispersion_parameter=16.8,
#                                     dz=1)

wdm = update_wdm_parameters_from_json(wdm_json)
channel = update_channel_parameters_from_json(channel_json)


# main function to optimise

def f_minimise(x, args):

    # x - values for optimisation - cutted spectrum with 2n real values
    # and only left half of the spectrum
    x = np.array(x)

    start_time = datetime.now()

    channel = args[0]
    wdm = args[1]
    points_x = args[2]
    points_y = args[3]
    spectrum_size = args[4]
    return_type = args[5]
    is_odd = args[6]
    split = args[7]

    # decompose to complex numbers and restore full spectrum from half
    if split:
        cut_spectrum = restore_array(decompose(x), is_odd=is_odd)
    else:
        cut_spectrum = decompose(x)

    # for w-space
    spectrum = spectrum_add_zero_sides(cut_spectrum, spectrum_size)
    filter_values = ifft(ifftshift(spectrum))

    n_t = wdm['upsampling'] * wdm['n_symbols']
    filter_upsampled = upsample_zero_sides(filter_values, n_t)
    filter_upsampled = tf.cast(filter_upsampled, tf.complex128)
    filter_upsampled_conj = tf.cast(np.conjugate(filter_upsampled), tf.complex128)

    ft_filter_values = tf.signal.fftshift(tf.signal.fft(filter_upsampled))
    ft_filter_values_conj = tf.signal.fftshift(tf.signal.fft(filter_upsampled_conj))

    norm_coef = tf.reduce_mean(tf.math.pow(tf.math.abs(ft_filter_values), 2))
    print("Normalisation coefficient of ft_filter_values", norm_coef)

    ft_filter_values = ft_filter_values * tf.cast(tf.math.sqrt(wdm['upsampling'] / norm_coef), tf.complex128)

    # result = ch.full_line_model_optimise(channel, wdm, points_x, points_y, ft_filter_values, ft_filter_values_conj, return_type=return_type)

    ft_filter_tx = [[ft_filter_values for _ in range(wdm['n_channels'])] for _ in range(2)]
    ft_filter_rx = [[ft_filter_values_conj for _ in range(wdm['n_channels'])] for _ in range(2)]

    result = full_line_model_wdm(channel, wdm, verbose=2, optimise=return_type, channels_type='middle',
                                 ft_filter_values_tx=ft_filter_tx,
                                 ft_filter_values_rx=ft_filter_rx)

    end_time = datetime.now()
    time_diff = (end_time - start_time)
    execution_time = time_diff.total_seconds() * 1000
    print("Function evaluation took", execution_time, "ms")

    return result


# create wrapper for minimisation function
def f_minimise_gen(x, args):
    return f_minimise(x, args),  # must return a tuple

# x0 our initial approximation


def add_noise_to_array(array, noise_scale=0.01):
    # Create random noise in the range [-noise_scale/2, noise_scale/2]
    noise = np.random.uniform(-noise_scale/2, noise_scale/2, array.shape)
    # Add the noise to the original array
    noisy_array = array + noise
    return noisy_array


def load_best_individual(filename):
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            states = pickle.load(f)

        if len(states) > 0:
            # Get the last state
            last_state = states[-1]

            # Extract the best individual
            best_individual = last_state['best_individual']

            return best_individual
        else:
            print("No states found in the file.")
            return None
    else:
        print("State file does not exist.")
        return None


# allocate GPU memory
gpus = tf.config.list_physical_devices('GPU')
if gpus:
    # Restrict TensorFlow to only allocate 4GB of memory on the first GPU
    try:
        tf.config.set_logical_device_configuration(
            gpus[GPU_NUM],
            [tf.config.LogicalDeviceConfiguration(memory_limit=GPU_MEM_LIM)])
        logical_gpus = tf.config.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Virtual devices must be set before GPUs have been initialized
        print(e)


try:
    # Specify an invalid GPU device
    with tf.device('/device:GPU:' + str(GPU_NUM)):

        # create optimisation parameters
        ft_filter_size = 2 ** 10  # size of optimized spectrum
        bandwidth_frac = 1.1

        init_filter_to_optimize = rrcosfilter(ft_filter_size, wdm['roll_off'], 1. / wdm['symb_freq'], wdm['sample_freq'])
        ft_init_filter = tf.signal.fftshift(tf.signal.fft(init_filter_to_optimize))
        freq = fftshift(fftfreq(ft_filter_size, d=1. / wdm['sample_freq']))
        init_cut_spectrum = get_spec_limited(ft_init_filter.numpy(), freq, wdm['symb_freq'] * bandwidth_frac)
        freq_cut = get_spec_limited(freq, freq, wdm['symb_freq'] * bandwidth_frac)
        print('Efficient number of points to optimise:', len(init_cut_spectrum))


        # we need to create class to be able to load
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMin)

        best_individual = load_best_individual(filename_load)
        if load_flag and (best_individual is not None):
            x0 = best_individual
        else:
            # set initial approximation
            if len(init_cut_spectrum) % 2 == 0:
                is_odd = False
            else:
                is_odd = True

            if split:
                x0 = compose(split_array(init_cut_spectrum))
            else:
                x0 = compose(init_cut_spectrum)


        def initial_value_function():
            return add_noise_to_array(x0, noise_scale=0.01)

        n_point_x = len(x0)

        # print(type(x0), type(init_cut_spectrum), np.shape(x0), np.shape(init_cut_spectrum))
        print('Check x0 mean ->', np.mean(x0), 'with len', n_point_x)  # I don't why but we have to do smt with x0 or it will be nan


        # main part


        toolbox = base.Toolbox()
        # Initialization functions
        toolbox.register("attr_float", initial_value_function)  # or any other initialization function suitable for your problem
        # toolbox.register("attr_float", np.random.random)  # or any other initialization function suitable for your problem
        # toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_float, n=n_point_x)  # n is the length of the individual

        def create_individual():
            return creator.Individual(add_noise_to_array(x0, noise_scale=0.01))
        toolbox.register("individual", create_individual)

        toolbox.register("population", tools.initRepeat, list, toolbox.individual)

        args = (channel, wdm, None, None, ft_filter_size, 'evm_x', is_odd, split)

        toolbox.register("evaluate", lambda x: f_minimise_gen(x, args))  # function to evaluate an individual's fitness
        toolbox.register("mate", tools.cxTwoPoint)  # crossover (mating) operation
        toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=1, indpb=0.1)  # indpb represents the probability that each attribute (or 'gene') of an individual will be mutated
        # toolbox.register("select", tools.selTournament, tournsize=3)  # moderate selection pressure
        toolbox.register("select", tools.selTournament, tournsize=10)  # high selection pressure


        def main():
            pop = toolbox.population(n=100)  # Initialize population. Adjust size as necessary.
            CXPB = 0.5  # Crossover probability with which two individuals will exchange part of their structure
            MUTPB = 0.05  # Mutation probability. Mutation is a background operator to maintain diversity in the population
            NGEN = 5000  # Number of generations (iterations)
            ELITISM_SIZE = 3  # number of best individuals to keep from one generation to the next

            # Evaluate the entire population
            # fitnesses = list(map(toolbox.evaluate, pop))
            fitnesses = []
            for individual in pop:
                fitness = toolbox.evaluate(individual)
                fitnesses.append(fitness)

            for ind, fit in zip(pop, fitnesses):
                ind.fitness.values = fit

            # Keep track of the best individual and its fitness.
            best_individual = None
            best_fitness = None

            # Keep track of the average fitness at each generation.
            avg_fitnesses = []
            best_fitnesses = []

            SAVE_INTERVAL = 2  # Save every 1 generations.

            # Evolution loop
            for g in range(NGEN):
                print("-- Generation %i --" % g)
                # Select the next generation individuals
                offspring = toolbox.select(pop, len(pop) - ELITISM_SIZE)
                # Clone the selected individuals
                offspring = list(map(toolbox.clone, offspring))

                # Apply crossover and mutation on the offspring
                for child1, child2 in zip(offspring[::2], offspring[1::2]):
                    if np.random.random() < CXPB:
                        toolbox.mate(child1, child2)
                        del child1.fitness.values
                        del child2.fitness.values

                for mutant in offspring:
                    if np.random.random() < MUTPB:
                        toolbox.mutate(mutant)
                        del mutant.fitness.values

                # Evaluate the individuals with an invalid fitness
                invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
                fitnesses = map(toolbox.evaluate, invalid_ind)
                for ind, fit in zip(invalid_ind, fitnesses):
                    ind.fitness.values = fit

                # Add the best back to population:
                offspring.extend(tools.selBest(pop, ELITISM_SIZE))

                # Replace population
                pop[:] = offspring

                # Calculate average fitness of the current generation.
                avg_fitness = np.mean([ind.fitness.values[0] for ind in pop])
                avg_fitnesses.append(avg_fitness)
                best_fitness = np.min([ind.fitness.values[0] for ind in pop])
                best_fitnesses.append(best_fitness)

                # Update the best individual found so far.
                for ind in pop:
                    if best_fitness is None or ind.fitness.values[0] < best_fitness:
                        best_individual = ind
                        best_fitness = ind.fitness.values[0]

                # Save the state of the GA every SAVE_INTERVAL generations.
                if g % SAVE_INTERVAL == 0:
                    save_state(filename, pop, best_individual, avg_fitnesses, g)
                    plot_and_save(avg_fitnesses, best_fitnesses, filename_plot)

            return pop


        if __name__ == "__main__":
            pop = main()
            best_ind = tools.selBest(pop, 1)[0]
            print("Best individual is %s, %s" % (best_ind, best_ind.fitness.values))

except RuntimeError as e:
    print(e)
