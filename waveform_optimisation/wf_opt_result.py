import numpy as np
import scipy as sp
import tensorflow as tf
import commpy
import pickle

import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams.update({'font.size': 20})

# import signal_generation as sg
# import waveform_optimiser as wf
import channel_model as ch

from scipy.fft import fft, ifft, fftshift, ifftshift, fftfreq
from scipy.optimize import minimize, NonlinearConstraint
from importlib import reload
from datetime import datetime

data_dir = 'data/'
job_name = 'test'
n_max_iter = 1

# create parameters
wdm = ch.create_wdm_parameters(n_channels=1, p_ave_dbm=6, n_symbols=2 ** 15, m_order=16, roll_off=0.1, upsampling=16,
                               downsampling_rate=1, symb_freq=34e9, channel_spacing=75e9, n_polarisations=2)

channel = ch.create_channel_parameters(n_spans=12,
                                       z_span=80,
                                       alpha_db=0.2,
                                       gamma=1.2,
                                       noise_figure_db=-200,
                                       dispersion_parameter=16.8,
                                       dz=1)

# create points which we will use

p_ave_x_dbm = wdm['p_ave_dbm']  # dBm
p_ave_x = (10 ** (p_ave_x_dbm / 10)) / 1000 / 2
modem_x = commpy.QAMModem(wdm['m_order'])
n_bits_x = int(modem_x.num_bits_symbol * wdm['n_symbols'])
bits_x = np.random.randint(0, 2, n_bits_x, int)  # Random bit stream
gray_x = ch.binarytoGray(bits_x, modem_x.num_bits_symbol)  # after gray code

points_x = modem_x.modulate(gray_x) / np.sqrt(modem_x.Es)  # Modulated baud points sQ = mod1.modulate(sB)/np.sqrt(mod1.Es)
points_x = points_x * np.sqrt(p_ave_x)

p_ave_y_dbm = wdm['p_ave_dbm']  # dBm
p_ave_y = (10 ** (p_ave_y_dbm / 10)) / 1000 / 2
modem_y = commpy.QAMModem(wdm['m_order'])
n_bits_y = int(modem_y.num_bits_symbol * wdm['n_symbols'])
bits_y = np.random.randint(0, 2, n_bits_y, int)  # Random bit stream
gray_y = ch.binarytoGray(bits_y, modem_y.num_bits_symbol)  # after gray code

points_y = modem_y.modulate(gray_y) / np.sqrt(modem_y.Es)  # Modulated baud points sQ = mod1.modulate(sB)/np.sqrt(mod1.Es)
points_y = points_y * np.sqrt(p_ave_y)

print('Points was created')

# create RRC filter as first approximation

n_t = wdm['upsampling'] * wdm['n_symbols']
ft_filter_values = tf.signal.fftshift(tf.signal.fft(ch.rrcosfilter_our(n_t, wdm['roll_off'], 1 / wdm['symb_freq'], wdm['sample_freq'])))
ft_filter_values = tf.cast(ft_filter_values, tf.complex128)

print('Initial filter was created')

def compose(x_c):
    # x_c -> x_r
    # make a real valued array of size 2n
    n = len(x_c)
    x_r = np.zeros(2 * n)
    x_r[:n] = np.real(x_c)
    x_r[n:] = np.imag(x_c)

    return x_r


def decompose(x_r):
    # x_r -> x_c
    n = len(x_r) // 2
    x_c = np.zeros(n, dtype=np.complex128)
    x_c = x_r[:n] + 1j * x_r[n:]
    return x_c


def upsample_zero_sides(f, new_size):

    n = len(f)
    zeros = np.zeros(int((new_size - n) / 2), dtype=np.complex128)
    return np.concatenate((zeros, f, zeros))


def spectrum_add_zero_sides(spectrum, new_size):

    n = len(spectrum)
    n_add = int((new_size - n) / 2)
    if n % 2 == 0:
        zeros_left = np.zeros(n_add, dtype=np.complex128)
        zeros_right = np.zeros(n_add, dtype=np.complex128)
    else:
        zeros_left = np.zeros(n_add + 1, dtype=np.complex128)
        zeros_right = np.zeros(n_add, dtype=np.complex128)
    return np.concatenate((zeros_left, spectrum, zeros_right))


def get_spec_limited(spectrum, freq, bandwidth):
    ind = np.where(np.logical_and(freq >= -bandwidth / 2, freq <= bandwidth / 2))
    return spectrum[ind]



start_time = datetime.now()
print("Run initial channel")
result_original = ch.full_line_model_optimise(channel, wdm, points_x, points_y, ft_filter_values, ft_filter_values, return_type='all')
end_time = datetime.now()
time_diff = (end_time - start_time)
execution_time = time_diff.total_seconds() * 1000
print("Function evaluation took", execution_time, "ms")

ft_filter_size = 2 ** 10 # size of optimized spectrum
bandwidth_frac = 1.2
init_filter_to_optimize = ch.rrcosfilter_our(ft_filter_size, wdm['roll_off'], 1. / wdm['symb_freq'], wdm['sample_freq'])
ft_init_filter = sp.fft.fftshift(sp.fft.fft(init_filter_to_optimize))
freq = fftshift(sp.fft.fftfreq(ft_filter_size, d=1. / wdm['sample_freq']))

# Load results

x_opt = np.load(data_dir + "data_wf_opt6_dbm_12_span_run_1.npy", allow_pickle=True)
cut_spectrum = decompose(x_opt)
# for w-space
spectrum = spectrum_add_zero_sides(cut_spectrum, ft_filter_size)
filter = ifft(ifftshift(spectrum))

n_t = wdm['upsampling'] * wdm['n_symbols']
filter_upsampled = upsample_zero_sides(filter, n_t)
filter_upsampled = tf.cast(filter_upsampled, tf.complex128)

ft_filter_values_opt = tf.signal.fftshift(tf.signal.fft(filter_upsampled))

start_time = datetime.now()
print("Run optimised channel")
result_opt = ch.full_line_model_optimise(channel, wdm, points_x, points_y, ft_filter_values_opt, ft_filter_values_opt, return_type='all')
end_time = datetime.now()
time_diff = (end_time - start_time)
execution_time = time_diff.total_seconds() * 1000
print("Function evaluation took", execution_time, "ms")

points_x_shifted = result_opt['points_x_shifted']
points_y_shifted = result_opt['points_y_shifted']
points_orig_x = result_opt['points_orig_x']
points_orig_y = result_opt['points_orig_y']
points_x = result_opt['points_x']
points_y = result_opt['points_y']


fig, axs = plt.subplots(3, 2, figsize=(15, 15))

# for k_ind in [0, 1, 2]:
for k_ind in [0]:
    # axs[0][0].scatter(points_x[k_ind].real, points_x[k_ind].imag, s=12, c='r', marker='x')
    axs[k_ind][0].scatter(points_x_shifted[k_ind].real, points_x_shifted[k_ind].imag, s=12, c='b', marker='x')
    axs[k_ind][0].scatter(points_orig_x[k_ind].real, points_orig_x[k_ind].imag, s=6, c='r', marker='.')
    axs[k_ind][0].grid(True)

    # axs[0][1].scatter(points_y[k_ind].real, points_y[k_ind].imag, s=12, c='r', marker='x')
    axs[k_ind][1].scatter(points_y_shifted[k_ind].real, points_y_shifted[k_ind].imag, s=12, c='b', marker='x')
    axs[k_ind][1].scatter(points_orig_y[k_ind].real, points_orig_y[k_ind].imag, s=6, c='r', marker='.')
    axs[k_ind][1].grid(True)


f_coef = 10.0
fig, axs = plt.subplots(2, 1, figsize=(15, 10))
axs[0].plot(freq, np.absolute(spectrum), linewidth=5, color='blue')
axs[0].plot(freq, np.absolute(ft_init_filter), linewidth=5, color='green')
axs[0].set_xlim(-f_coef * wdm['symb_freq'], f_coef * wdm['symb_freq'])
axs[0].grid(True)

axs[1].plot(freq, np.real(spectrum), linewidth=5, color='blue')
axs[1].plot(freq, np.imag(spectrum), linewidth=5, color='green')
axs[1].set_xlim(-f_coef * wdm['symb_freq'], f_coef * wdm['symb_freq'])
axs[1].grid(True)


# print(np.absolute(np.mean(points_orig_x[k_ind]) / np.mean(points_x[k_ind])), np.absolute(np.mean(points_orig_y[k_ind]) / np.mean(points_y[k_ind])))

# init_filter_to_optimize = ch.rrcosfilter_our(ft_filter_size, wdm['roll_off'], 1. / wdm['symb_freq'], wdm['sample_freq'])
# ft_init_filter = tf.signal.fftshift(tf.signal.fft(init_filter_to_optimize))
# freq = fftshift(fftfreq(ft_filter_size, d=1. / wdm['sample_freq']))
# init_cut_spectrum = get_spec_limited(ft_init_filter.numpy(), freq, wdm['symb_freq'] * bandwidth_frac)
# freq_cut = get_spec_limited(freq, freq, wdm['symb_freq'] * bandwidth_frac)
# print('Efficient number of points to optimise:', len(init_cut_spectrum))


# # set initial approximation
# x0 = compose(init_cut_spectrum)
#
# # optimise
# res = minimize(f_minimise,
#                method='BFGS',
#                x0=x0,
#                args=[channel, wdm, points_x, points_y, ft_filter_size, 'evm_x'],
#                callback=callback,
#                options={'maxiter': n_max_iter, 'disp': True})
#
# # write result to file
# with open(data_dir + 'result_wf_opt_' + job_name + '.pkl', 'wb') as output:
#     pickle.dump(res, output, pickle.HIGHEST_PROTOCOL)

