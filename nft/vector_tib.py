import numpy as np

def vdtib(r1, r2, t):

    sigma = -1  # focusing / defocusing case = -1 / +1

    dt = t[1] - t[0]
    n = len(t)
    q1 = np.zeros(n, dtype=complex)
    q2 = np.zeros(n, dtype=complex)

    # first step

    q1[0] = -2.0 * r1[0]
    q2[0] = -2.0 * r2[0]

    r1[0] = r1[0] / 2.0
    r2[0] = r2[0] / 2.0

    y_prev = np.array([1.0 / (1.0 -
                              sigma * r1[0] * np.conj(r1[0]) * dt * dt -
                              sigma * r2[0] * np.conj(r2[0]) * dt * dt)
                       ])

    # elements for inverse matrix
    z1_prev = np.array([-dt * r1[0] * y_prev[0]])
    z2_prev = np.array([-dt * r2[0] * y_prev[0]])

    b1 = r1[1] * y_prev
    b2 = r2[1] * y_prev

    q1[1] = -2.0 * b1
    q2[1] = -2.0 * b2

    for m in range(2, n):

        c_m = 1.0 / (1.0 -
                     sigma * b1 * np.conj(b1) * dt * dt -
                     sigma * b2 * np.conj(b2) * dt * dt)

        d1_m = -dt * b1 * c_m
        d2_m = -dt * b2 * c_m

        y_next = c_m * np.append(y_prev, 0.) + \
                 sigma * d1_m * np.conj(np.append(0., z1_prev[::-1])) + \
                 sigma * d2_m * np.conj(np.append(0., z2_prev[::-1]))

        z1_next = c_m * np.append(z1_prev, 0.) + d1_m * np.conj(np.append(0., y_prev[::-1]))
        z2_next = c_m * np.append(z2_prev, 0.) + d2_m * np.conj(np.append(0., y_prev[::-1]))

        b1 = np.sum(r1[m:0:-1] * y_next)
        b2 = np.sum(r2[m:0:-1] * y_next)

        q1[m] = -2.0 * b1
        q2[m] = -2.0 * b2

        y_prev = y_next
        z1_prev = z1_next
        z2_prev = z2_next

    return q1, q2
