#!/usr/bin/env python
# coding: utf-8

# In[53]:


import numpy as np
import matplotlib.pyplot as mtp

J0 = -1                                    #параметр фокус/дефокус. Фокус при -1
L = 20                                     #длина промежутка
M = 11; N = 2**M;                          #N - кол-во точек

dx = 2*L/N;                                #шаг ядра

x_pot = np.linspace(-L/2,L/2-dx/2,N)       #сетка потенциала
x_kernel = 2*x_pot                         #сетка ядра

phi = np.pi/6                                  #угол поляризации
theta1 = np.pi/4; theta2 = np.pi/4+np.pi;      #фазы одной и другой поляризации солитона
w = -0.2/L; beta = 10/L;                       #параметр "частоты" и параметр амплитуды
delta = L*beta/4                               #сдвиг солитона

q1_0 = 2*beta*np.cos(phi)*np.exp(-2j*w*x_pot+1j*theta1)/np.cosh(2*beta*x_pot + delta)    #поляризация солитона 1
q2_0 = 2*beta*np.sin(phi)*np.exp(-2j*w*x_pot+1j*theta2)/np.cosh(2*beta*x_pot + delta)    #поляризация солитона 2

#mtp.figure()
#mtp.plot(x_pot,np.real(q1),x_pot,np.imag(q1))
#mtp.figure()
#mtp.plot(x_pot,np.real(q2),x_pot,np.imag(q2))

k = 1j*beta+w;
m1 = 2*beta*np.cos(phi)*np.exp(delta+1j*theta1);        #конструирование коэф номрировки 1
m2 = 2*beta*np.sin(phi)*np.exp(delta+1j*theta2);        #конструирование коэф номрировки 2

R1 = -m1*np.exp(-1j*k*x_kernel)                         #ядро 1
R2 = -m2*np.exp(-1j*k*x_kernel)                         #ядро 2

q1 = np.zeros(N)*1j; q2 = np.zeros(N)*1j;               #создание массивов для значений восстановленного потенциала

q1[0] = -2*R1[0]; q2[0] = -2*R2[0];                     #первый шаг алгоритма

R1[0] = R1[0]/2;  R2[0] = R2[0]/2;
y = (1 - J0*R1[0]*np.conj(R1[0])*dx*dx - J0*R2[0]*np.conj(R2[0])*dx*dx)**(-1);
y = np.array(y)
z1= -dx*R1[0]*y; z2 = -dx*R2[0]*y;                      #создание элементов обратной матрицы

bet1 = R1[1]*y; bet2 = R2[1]*y;                         #бета алгоритма для m=1
q1[1] = -2*bet1; q2[1] = -2*bet2;

for m in range(2,N):
    c_m = (1 - J0*bet1*np.conj(bet1)*dx*dx - J0*bet2*np.conj(bet2)*dx*dx)**(-1)
    d_m1 = -dx*bet1*c_m; d_m2 = -dx*bet2*c_m;
    y0 = np.append(y,[0]); z10 = np.append(z1,[0]); z20 = np.append(z2,[0]);
    
    y = c_m*y0 + J0*d_m1*np.flip(np.conj(z10)) + J0*d_m2*np.flip(np.conj(z20));
    z1= c_m*z10 + d_m1*np.flip(np.conj(y0));
    z2= c_m*z20 + d_m2*np.flip(np.conj(y0));
    
    bet1 = np.sum(R1[m:0:-1]*y)
    bet2 = np.sum(R2[m:0:-1]*y)
    
    q1[m]= -2*bet1; q2[m]= -2*bet2;

fig1 = mtp.figure();
mtp.plot(x_pot,np.real(q1),x_pot,np.real(q1_0),'--')

fig2 = mtp.figure();
mtp.plot(x_pot,np.imag(q1),x_pot,np.imag(q1_0),'--')

fig3 = mtp.figure();
mtp.plot(x_pot,np.real(q2),x_pot,np.real(q2_0),'--')

fig4 = mtp.figure();
mtp.plot(x_pot,np.imag(q2),x_pot,np.imag(q2_0),'--')

sigma = np.sqrt(np.sum(0.5*dx*(abs(q1-q1_0)**2+abs(q2-q2_0)**2)))
sigma


# In[ ]:




