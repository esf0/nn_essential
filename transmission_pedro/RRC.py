import numpy as np
from commpy.filters import rrcosfilter

class RRCfilter:
 def __init__(self, N, beta, Ts, Fs):  # t0 introduces a delay of 3Ts
    self.N = int(Fs*32*Ts*2) #int(Fs*3*Ts*2*4)  # Length of the filter in samples truncate it to a length of 2 t0 and (t0=3Ts) and shift t0 to make it causal
    self.beta = beta  # Roll off factor (Valid values are [0, 1]).
    self.Ts = Ts  # Symbol period in seconds.
    self.Fs = Fs  # Sampling Rate in Hz.

 def filter(self, sQ):
     self.sPSF = rrcosfilter(self.N, self.beta, self.Ts, self.Fs)[1]  # (N * 4, 0.8, 1, 24)[1]
     self.qW = np.convolve(sQ, self.sPSF)  # Waveform with PSF



