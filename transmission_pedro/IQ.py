import numpy as np

class IQMod:
 def __init__(self):  # t0 introduces a delay of 3Ts
    pass

 def Modulator(self, U,t_u,fc):
     self.I = U.real * np.cos(2*np.pi*t_u*fc)
     self.Q = U.imag * -np.sin(2*np.pi*t_u*fc)

 def Spectrum(self, U, Fs):
     self.ffLen = 4*len(U)  # perform 4-times zeropadding to get a smoother spectrum
     spec = lambda x: np.fft.fftshift(np.fft.fft(x, self.ffLen))/Fs*(len(U))
     self.specIa = spec(U.real)
     self.specQa = spec(U.imag)
     self.specId = spec(self.I)
     self.specQd = spec(self.Q)