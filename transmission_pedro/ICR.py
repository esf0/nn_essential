import numpy as np
import math
import scipy.signal
from RRC import *

class ICR(object):

    def __init__(self,signal_tx,fc, t_u):
        self.I = signal_tx*np.cos(2*np.pi*fc*t_u)
        self.Q = -signal_tx*np.sin(2*np.pi*fc*t_u)
        pass

    def lowfilter(self,lowpass_order, cutoff, Fs):
        lowpass = scipy.signal.firwin(lowpass_order, cutoff / (Fs / 2))
        self.IdownLP = scipy.signal.lfilter(lowpass, 1, self.I)
        self.QdownLP = scipy.signal.lfilter(lowpass, 1, self.Q)

    def receiver(self, numberOfsymbols, roll_off, Ts, Fs):
        v = self.IdownLP + 1j * self.QdownLP
        y = RRCfilter(numberOfsymbols, roll_off, Ts, Fs)
        y.filter(v)
        self.out = y.qW / ((sum(y.sPSF ** 2))) * 2

    def receiver2(self, signal_tx, roll_off, Ts, Fs):
         v = signal_tx
         y = RRCfilter(0, roll_off, Ts, Fs)
         y.filter(v)
         self.out2 = y.qW / ((sum(y.sPSF ** 2)))
