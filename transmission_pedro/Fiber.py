"""
General Numerical Solver for the 1D Time-Dependent Schrodinger's equation.

"""

import numpy as np
import cmath


class Schrodinger(object):

    def __init__(self, L_s, alfa, beta_2, gama, signal_tx, signal_ty):
        self.Ls = L_s
        self.alfa = alfa
        self.beta2 = beta_2
        self.gama = gama
        self.signalx = signal_tx
        self.signaly = signal_ty
        self.outputx = 0
        self.outputy = 0

    def Split_Step(self, N, Fs):
        alpha = self.alfa
        alph = alpha / 4.343  # page#55 eqn 2.5.3 agrawal
        gamma = self.gama
        b2 = self.beta2  # -((1550e-9) ** 2) * (DD * 1e-6) / (2 * np.pi * 3e8)  # equation 2.3.5 agrawal
        Ld = self.Ls
        dt = 1 / Fs
        h = Ld / N
        ux = self.signalx
        uy = self.signaly
        lx = np.max(ux.shape)
        ly = np.max(uy.shape)

        dwx = 1.0 / float(lx) / dt * 2.0 * np.pi
        dwy = 1.0 / float(ly) / dt * 2.0 * np.pi

        wx = dwx * np.arange(-1 * lx / 2.0, lx / 2.0, 1)
        wy = dwy * np.arange(-1 * ly / 2.0, ly / 2.0, 1)

        wx = np.asarray(wx)
        wx = np.fft.fftshift(wx)

        wy = np.asarray(wy)
        wy = np.fft.fftshift(wy)

        ux = np.asarray(ux)
        uy = np.asarray(uy)

        spectrumx = np.fft.fft((ux))
        spectrumy = np.fft.fft((uy))

        for jj in np.arange(N):
            spectrumx = spectrumx * np.exp(-alph / 2 * (h / 2.0) + 1j * b2 / 2.0 * (np.power(wx, 2)) * (h / 2.0))
            spectrumy = spectrumy * np.exp(-alph / 2 * (h / 2.0) + 1j * b2 / 2.0 * (np.power(wy, 2)) * (h / 2.0))
            fx = np.fft.ifft(spectrumx)
            fy = np.fft.ifft(spectrumy)
            a = fx
            b = fy
            fx = fx * np.exp(8 * 1j * gamma * (np.power(np.absolute(a), 2) + np.power(np.absolute(b), 2)) * (h) / 9)
            fy = fy * np.exp(8 * 1j * gamma * (np.power(np.absolute(a), 2) + np.power(np.absolute(b), 2)) * (h) / 9)
            spectrumx = np.fft.fft(fx)
            spectrumy = np.fft.fft(fy)
            spectrumx = spectrumx * np.exp(-alph / 2 * (h / 2.0) + 1j * b2 / 2.0 * (np.power(wx, 2) * (h / 2.0)))
            spectrumy = spectrumy * np.exp(-alph / 2 * (h / 2.0) + 1j * b2 / 2.0 * (np.power(wy, 2) * (h / 2.0)))

        self.outputx = np.fft.ifft(spectrumx)
        self.outputy = np.fft.ifft(spectrumy)
        self.w = wx
        self.wy = wy


""""
    h=self.Ls/N
      ii = complex(0, 1)
      xs =(np.fft.fft(self.signalx))
      wx=299792458*2*np.pi/1550e-9+np.fft.fftfreq(len(xs))*2*np.pi*Fs
      ys =(np.fft.fft(self.signaly))
      wy=299792458*2*np.pi/1550e-9+np.fft.fftfreq(len(ys))*2*np.pi*Fs


      self.outputx  = self.signalx
      self.outputy  = self.signaly
      dB2Neper = 1/4.343
      for i in range(1, N+1):
       aux_halflinear1x = (np.fft.ifft(np.exp((h/2)*((-self.alfa*dB2Neper/2) + ii*(self.beta2*(wx**2))/2))*((np.fft.fft(self.outputx)))))
       aux_halflinear1y = (np.fft.ifft(np.exp((h/2)*((-self.alfa*dB2Neper/2) + ii*(self.beta2*(wy**2))/2))*((np.fft.fft(self.outputy)))))
       aux_nonlinearx = np.exp(h*ii*self.gama*(8/9)*((np.absolute(aux_halflinear1x)**2)+(np.absolute(aux_halflinear1y)**2)))*aux_halflinear1x
       aux_nonlineary = np.exp(h*ii*self.gama*(8/9)*((np.absolute(aux_halflinear1x)**2)+(np.absolute(aux_halflinear1y)**2)))*aux_halflinear1y
       aux_halflinear2x = (np.fft.ifft(np.exp((h/2)*((-self.alfa*dB2Neper/2) + ii*(self.beta2*(wx**2))/2))*((np.fft.fft(aux_nonlinearx)))))
       aux_halflinear2y = (np.fft.ifft(np.exp((h/2)*((-self.alfa*dB2Neper/2) + ii*(self.beta2*(wy**2))/2))*((np.fft.fft(aux_nonlineary)))))
       self.outputx = aux_halflinear2x
       self.outputy = aux_halflinear2y


"""

# self.output = np.exp((h/2)*ii*self.gama*np.absolute(self.signal)**2)*self.signal
# for i in range(1, N+1):
# aux_halflinear1 = (np.fft.ifft(np.exp(h*((-self.alfa*dB2Neper/2) + ii*(self.beta2*(w**2))/2))*((np.fft.fft(self.output)))))
# aux_nonlinear = np.exp(h*ii*self.gama*np.absolute(aux_halflinear1)**2)*aux_halflinear1
# self.output = aux_nonlinear
# self.output = np.exp(-(h/2)*ii*self.gama*np.absolute(self.output)**2)*self.output
