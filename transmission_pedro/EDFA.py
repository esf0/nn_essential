import numpy as np
import math


class AmpEDFA(object):

    def __init__(self, NF, G, signalx, signaly, h, freq):
        self.NF = NF
        self.G = G
        self.h = h
        self.freq = freq
        self.signalx = signalx
        self.signaly = signaly
        self.output = 0
        self.noise = 0
        self.PSD = 0

    def result(self, band):
        self.PSD = ((10 ** (self.NF / 10)) * (10 ** ((self.G) / 10)) - 1) * self.h * self.freq
        std = np.sqrt(self.PSD * band)
        self.noise1x = np.random.normal(0, 1, size=len(self.signalx))
        self.noise2x = np.random.normal(0, 1, size=len(self.signalx))
        noise_complex = (self.noise1x + 1j * self.noise2x) / np.sqrt(2)

        self.noise1y = np.random.normal(0, 1, size=len(self.signalx))
        self.noise2y = np.random.normal(0, 1, size=len(self.signalx))
        noise_complexy = (self.noise1y + 1j * self.noise2y) / np.sqrt(2)

        self.outputx = math.sqrt(10 ** (self.G / 10)) * self.signalx + noise_complex * std
        self.outputy = math.sqrt(10 ** (self.G / 10)) * self.signaly + noise_complexy * std
