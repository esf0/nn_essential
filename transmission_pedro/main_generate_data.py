#  ######## SET THE Libraries #########
from IQ import *
from QoT import *
from Fiber import *
from EDFA import *
from ICR import *
from detectors import *
import matplotlib.pyplot as plt
from Neuron import *
import scipy.signal
import pdb
import BER_calc


def BER_est(x, x_ref):
    QAM_order = M
    return BER_calc.QAM_BER_gray(x, x_ref, QAM_order)


# pdb.set_trace()
#  ######## SET THE QAM PROPERTIES #########
for index in range(1):

    Pot = 5  # PP[index]
    opt = [0, 1]
    N = 2 ** 17  # Number of Symbols transmitted 1024
    numberOfsymbols = N
    M = 16  # Modulation Level
    roll_off = 0.1  # 1
    Fs = int(64e9 * 8)  # sampling frequency used for the discrete simulation of analog signals
    s = int(64e9)  # symbol frequency
    fc = 299792458 / 1550e-9  # carrier frequency
    Ts = 1 / s  # symbol spacing
    BN = 1 / (2 * Ts)  # Nyquist bandwidth of the base band signal
    ups = int(Ts * Fs)  # Number of samples per second in the analog domain
    filtlen = 2 ** 12  # Filter    length in symbols


    ####### Gray code ##########

    # Helper function to xor two characters
    def xor_c(a, b):
        return int(0) if (a == b) else int(1)


    # Helper function to flip the bit
    def flip(c):
        return int(1) if (c == int(0)) else int(0)


    # function to convert binary string
    # to gray string
    def binarytoGray(binary, num_bits_symbol):
        gray = np.zeros((len(binary),), dtype=int)
        # MSB of gray code is same as
        # binary code
        NN = int(len(binary) / num_bits_symbol)
        count = 0
        for ij in range(0, NN):
            gray[count] = binary[count]
            for j in range(count + 1, count + num_bits_symbol):
                gray[j] = xor_c(binary[j - 1], binary[j])
            count = count + num_bits_symbol
        return gray


    # function to convert gray code
    # string to binary string
    def graytoBinary(gray, num_bits):
        NN = int(len(gray) / num_bits)
        binary = np.zeros((len(gray),), dtype=int)
        count = 0
        for ijij in range(0, NN):
            binary[count] = gray[count]
            for j in range(count + 1, count + num_bits):
                if gray[j] == 0:
                    binary[j] = binary[j - 1]
                else:
                    binary[j] = flip(binary[j - 1])
            count = count + num_bits
        return binary


    #  ######## INITIATE I, Q and noise components of polarization X #########
    Pot_Inicial_x = Pot  # dBm
    mod1 = QAMModem(M)
    aa = int(mod1.num_bits_symbol * N)
    X = np.random.randint(0, 2, aa, int)  # Random bit stream
    sB = binarytoGray(X, mod1.num_bits_symbol)  # after gray code
    sQ = mod1.modulate(sB) / np.sqrt(mod1.Es)  # Modulated baud points sQ = mod1.modulate(sB)/np.sqrt(mod1.Es)
    sQ = sQ * np.sqrt((10 ** (Pot_Inicial_x / 10)) / 1000)

    #  ######## INITIATE I, Q and noise components of polarization Y #########
    Pot_Inicial_y = Pot  # dBm
    mod2 = QAMModem(M)
    aay = int(mod2.num_bits_symbol * N)
    X_y = np.random.randint(0, 2, aay, int)  # Random bit stream
    sB_y = binarytoGray(X_y, mod2.num_bits_symbol)  # after gray code
    sQ_y = mod2.modulate(sB_y) / np.sqrt(mod2.Es)  # Modulated baud points sQ = mod1.modulate(sB)/np.sqrt(mod1.Es)
    sQ_y = sQ_y * np.sqrt((10 ** (Pot_Inicial_y / 10)) / 1000)

    #  ######## Turning the Discrete signal in Countinuous  X #########

    xxx = np.zeros(ups * N, dtype='complex')
    xxx[::ups] = sQ  # every ups samples, the value of sQ is inserted into the sequence
    t_x = np.arange(len(xxx)) / Fs

    #  ######## Turning the Discrete signal in Countinuous  Y #########

    yyy = np.zeros(ups * N, dtype='complex')
    yyy[::ups] = sQ_y  # every ups samples, the value of sQ is inserted into the sequence
    t_y = np.arange(len(yyy)) / Fs

    #  ######## Root Raised Cosine Filter X #########

    FIR = RRCfilter(filtlen, roll_off, Ts, Fs)

    FIR.filter(xxx)
    t_u = np.arange(len(FIR.qW)) / Fs

    #  ######## Root Raised Cosine Filter Y #########

    FIR_y = RRCfilter(filtlen, roll_off, Ts, Fs)

    FIR_y.filter(yyy)
    t_uy = np.arange(len(FIR_y.qW)) / Fs

    #  ######## Signal Transmitted  X #########

    signal_tx = FIR.qW

    #  ######## Signal Transmitted  X #########

    signal_ty = FIR_y.qW

    #  ######### Physical Layer Model #########

    N_s = 5  # Number of spans
    L_s = [80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000]  # Span Length [m]
    alfa = [0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3,
            0.22e-3]  # Attenuation C oefficient [dB/m]
    gama = [1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3,
            1.2e-3]  # Non-linear Coefficient [W^-1 m^-1]
    NF = [4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5]  # Noise Figure [dB]
    G = [17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6]  # Gain [dB]
    DD = 16.8 # // ps/nm/km  dispersion parameter
    bb = -(1550e-9 ** 2) * (DD * 1e-6) / (2 * np.pi * 3e8)  # equation 2.3.5 agrawal
    beta_2 = [bb, bb, bb, bb, bb, bb, bb, bb, bb, bb]  # Chromatic Dispersion Coefficient [s^2·m^−1]
    hh = 6.6256e-34  # Planck's constant [J/s]
    nu = 299792458 / 1550e-9  # light frequency carrier [Hz]

    aux = signal_tx
    auy = signal_ty
    ii = complex(0, 1)
    w = 0
    wy = 0

    for iii in range(25):
        fibre = Schrodinger(80000, 0.2e-3, 0, 1.2e-3, aux, auy)
        fibre.Split_Step(1, Fs)
        Signal = AmpEDFA(4.5, 17.6, fibre.outputx, fibre.outputy, hh, nu)
        Signal.result(Fs)
        aux = Signal.outputx
        auy = Signal.outputy
        w = fibre.w
        wy = fibre.wy


    #  Dispersion compensation X #

    signal_end = (np.fft.ifft((np.fft.fft(aux)) * np.exp(-L_s[1] * N_s * ii * (bb * (w ** 2)) / 2)))

    #  Dispersion compensation Y #

    signal_end_y = (np.fft.ifft((np.fft.fft(auy)) * np.exp(-L_s[1] * N_s * ii * (bb * (wy ** 2)) / 2)))

    #   ########  Receptor X #############
    recep = ICR(signal_end, fc, t_u)
    recep.receiver2(signal_end, roll_off, Ts, Fs)
    Out_MatchedFilter = recep.out2
    delay = int((2 * (32 * Ts)) * Fs)

    #   ########  Receptor Y #############
    recepy = ICR(signal_end_y, fc, t_u)

    recepy.receiver2(signal_end_y, roll_off, Ts, Fs)

    Out_MatchedFiltery = recepy.out2

    #  ##### Finding the complex Numbers in the Receiver X ########

    x = Out_MatchedFilter[delay:delay + N * ups:ups]  # every ups samples, the value of sQ is inserted into the sequence
    nl_shift = np.dot(np.transpose(np.conjugate(sQ)), sQ) / np.dot(np.transpose(np.conjugate(sQ)), x)
    x_shifted = x * nl_shift
    xx = x_shifted

    #  ##### Finding the complex Numbers in the Receiver Y ########

    y = Out_MatchedFiltery[
        delay:delay + N * ups:ups]  # every ups samples, the value of sQ is inserted into the sequence
    nl_shifty = np.dot(np.transpose(np.conjugate(sQ_y)), sQ_y) / np.dot(np.transpose(np.conjugate(sQ_y)), y)
    y_shifted = y * nl_shifty
    yy = y_shifted

    #  ##### Plots Received Constellations ########

    axs = plt
    axs.scatter(xx.real * np.sqrt(mod1.Es) / np.sqrt((10 ** (Pot_Inicial_x / 10)) / 1000),
                xx.imag * np.sqrt(mod1.Es) / np.sqrt((10 ** (Pot_Inicial_x / 10)) / 1000), s=8, c='r', marker='x')
    plt.xlabel("Ix component Received")
    plt.ylabel("Qx component Received")
    plt.grid()
    plt.show()

    axs = plt
    axs.scatter(yy.real * np.sqrt(mod2.Es) / np.sqrt((10 ** (Pot_Inicial_y / 10)) / 1000),
                yy.imag * np.sqrt(mod2.Es) / np.sqrt((10 ** (Pot_Inicial_y / 10)) / 1000), s=8, c='r', marker='x')
    plt.xlabel("Iy component Received")
    plt.ylabel("Qy component Received")
    plt.grid()
    plt.show()

    print(BER_est(xx, sQ))

