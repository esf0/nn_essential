import numpy as np
import math
import scipy.integrate as integrate
from scipy.special import erfinv


class Quality(object):

    def __init__(self):
        pass

    def BER (self, Info_tx, Info_rx):
     count = 0
     for i in range(0,len(Info_tx)):
        if Info_tx [i] != Info_rx [i] :
         count=count+1
     #self.ber =count/len(Info_tx)
     err = np.where(Info_tx != Info_rx)
     error_sum = float(len(err[0]))
     self.ber = error_sum / len(Info_tx)


    def EVM (self, Info_tx, Info_rx , mod):
     sigma_2 = 0
     maior = 0
     error = np.zeros(len(Info_tx), dtype=complex)
     if mod == 4 or mod == 8:
         k=1
     elif mod == 16:
         k = np.sqrt(9/5)
     elif mod == 32:
         k = np.sqrt(17/10)
     elif mod == 64:
         k = np.sqrt(7/3)

     for i in range(0,len(Info_tx)):
        error[i] = Info_rx[i] - Info_tx[i]
     for i in range(0,len(Info_tx)):
          sigma_2 = abs(error[i])**2 + sigma_2
     for i in range(0,len(Info_tx)):
        if maior < abs(Info_tx[i]):
           maior = abs(Info_tx[i])

     self.evm = np.sqrt(sigma_2/len(Info_tx))/maior

    def SNR (self, Info_tx, Info_rx):
        sigma_2 = 0
        avg = 0
        error = np.zeros(len(Info_tx), dtype=complex)
        for i in range(0,len(Info_tx)):
          error[i] = Info_rx[i] - Info_tx[i]
        for i in range(0,len(Info_tx)):
          sigma_2 = abs(error[i])**2 + sigma_2
        for i in range(0,len(Info_tx)):
          avg = abs(Info_tx[i]) + avg

        self.snr =10*np.log10((avg/len(Info_tx))/np.sqrt(sigma_2/len(Info_tx)))

    def Q (self, ber):
        self.qvalue =20*np.log10(np.sqrt(2)*erfinv(1-2*ber))


    """
         #  ######## SET THE Libraries #########

from IQ import *
from QoT import *
from Fiber import *
from EDFA import *
from ICR import *
from detectors import *
import matplotlib.pyplot as plt
import scipy.signal

#  ######## SET THE QAM PROPERTIES #########
for index in range(1):
    PP = [-6,-5,-4,-3, -2, -1, 0, 1, 2, 3, 4, 5, 6]
    roll = [0.25, 0.2, 0.15, 0.1, 0.05, 0.01]
    modulacaoatual = [16, 64]
    Pot = 3 #PP[index]
    opt = [0, 1]
    N = 2 ** 16  # Number of Symbols transmitted 1024
    numberOfsymbols = N
    M = 16 # Modulation Level
    roll_off = 0.1 # 1
    Fs = int(64e9 * 6)  # sampling frequency used for the discrete simulation of analog signals
    s = int(64e9)  # symbol frequency
    fc = 299792458 / 1550e-9  # carrier frequency
    Ts = 1 / s  # symbol spacing
    BN = 1 / (2 * Ts)  # Nyquist bandwidth of the base band signal
    ups = int(Ts * Fs)  # Number of samples per second in the analog domain


    ####### Gray code ##########

    # Helper function to xor two characters
    def xor_c(a, b):
        return int(0) if (a == b) else int(1);


    # Helper function to flip the bit
    def flip(c):
        return int(1) if (c == int(0)) else int(0);

    # function to convert binary string
    # to gray string
    def binarytoGray(binary, num_bits_symbol):
        gray = np.zeros((len(binary),), dtype=int)
        # MSB of gray code is same as
        # binary code
        N = int(len(binary) / num_bits_symbol)
        count = 0
        for i in range(0, N):
            gray[count] = binary[count]
            for j in range(count + 1, count + num_bits_symbol):
                gray[j] = xor_c(binary[j - 1], binary[j])
            count = count + num_bits_symbol
        return gray


    # function to convert gray code
    # string to binary string
    def graytoBinary(gray, num_bits):
        N = int(len(gray) / num_bits)
        binary = np.zeros((len(gray),), dtype=int)
        count = 0
        for i in range(0, N):
            binary[count] = gray[count]
            for j in range(count + 1, count + num_bits):
                if (gray[j] == 0):
                    binary[j] = binary[j - 1];
                else:
                    binary[j] = flip(binary[j - 1]);
            count = count + num_bits
        return binary;


    #  ######## INITIATE I, Q and noise components of polarization X #########
    Pot_Inicial_x = Pot  # dBm
    mod1 = QAMModem(M)
    aa = int(mod1.num_bits_symbol * N)
    X = np.random.randint(0, 2, aa, int)  # Random bit stream
    sB = binarytoGray(X, mod1.num_bits_symbol)  # after gray code
    sQ = mod1.modulate(sB) / np.sqrt(mod1.Es)  # Modulated baud points sQ = mod1.modulate(sB)/np.sqrt(mod1.Es)
    sQ = sQ * np.sqrt((10 ** (Pot_Inicial_x / 10)) / 1000)
    #print(np.max(np.abs(sQ)))

    #  ######## Turning the Discrete signal in Countinuous  X #########

    xxx = np.zeros(ups * N, dtype='complex')
    xxx[::ups] = sQ  # every ups samples, the value of sQ is inserted into the sequence
    t_x = np.arange(len(xxx)) / Fs

    #  ######## Root Raised Cosine Filter X #########

    FIR = RRCfilter(numberOfsymbols, roll_off, Ts, Fs)

    FIR.filter(xxx)
    t_u = np.arange(len(FIR.qW)) / Fs

    #  ######## IQ Modulator  X #########

    Mod = IQMod()
    Mod.Modulator(FIR.qW, t_u, fc)

    #  output of the IQ modulator X

    signal_tx = Mod.I + Mod.Q


    #  ######### Physical Layer Model #########

    N_s = 25 # Number of spans
    L_s = [80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000]  # Span Length [m]
    alfa = [0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3, 0.22e-3,
            0.22e-3]  # Attenuation Coefficient [dB/m]
    gama = [1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3, 1.2e-3,
            1.2e-3]  # Non-linear Coefficient [W^-1 m^-1]
    NF = [4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5]  # Noise Figure [dB]
    G = [17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6, 17.6]  # Gain [dB]
    # G = [22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22,22, 22 ]  # Gain [dB]
    DD = 17  # // ps/nm/km  dispersion parameter
    bb = -((1550e-9) ** 2) * (DD * 1e-6) / (2 * np.pi * 3e8)  # equation 2.3.5 agrawal
    beta_2 = [bb, bb, bb, bb, bb, bb, bb, bb, bb, bb]  # Chromatic Dispersion Coefficient [s^2·m^−1]
    hh = 6.6256e-34  # Planck's constant [J/s]
    nu = 299792458 / 1550e-9  # light frequency carrier [Hz]

    aux = signal_tx
    ii = complex(0, 1)
    w=0
    for x in range(N_s):
        fibre = Schrodinger(80000, 0.2e-3, bb, 1.2e-3, aux)
        fibre.Split_Step(80, Fs)
        Signal = AmpEDFA(4.5, 16, fibre.output, hh, nu)
        Signal.result(s)
        aux = Signal.output
        w = fibre.w

    #  Dispersion compensation X #

    # w=299792458*2*np.pi/1550e-9+np.fft.fftfreq(len(np.fft.fft(aux)))*2*np.pi*Fs
    signal_end = (np.fft.ifft((np.fft.fft(aux)) * np.exp(-L_s[1] * N_s * ii * (bb * (w ** 2)) / 2)))
    #signal_end = aux


    #   ########  Receptor X #############
    recep = ICR(signal_end, fc, t_u)

    #  ##### Low Pass Filter to eliminate the 2fc component ####

    cutoff = BN*6 # arbitrary design parameter
    lowpass_order = 51
    lowpass_delay = (lowpass_order // 2) / Fs  # A lowpass of order N, delays the signal by N/2 samples
    #  design filter
    lowpass = scipy.signal.firwin(lowpass_order, cutoff / (Fs / 2))
    #  frequency response of filter
    t_lp = np.arange(len(lowpass)) / Fs
    f_lp = np.linspace(-Fs / 2, Fs / 2, 2048, endpoint=False)
    H = np.fft.fftshift(np.fft.fft(lowpass, 2048))

    #  signal after filter X

    recep.lowfilter(lowpass_order, cutoff, Fs)
    idown_lp = recep.IdownLP
    qdown_lp = recep.QdownLP

    # Matched Filter  X

    recep.receiver(numberOfsymbols, roll_off, Ts, Fs)

    Out_MatchedFilter = recep.out

    delay = int((2 * (32* Ts) + lowpass_delay) * Fs)

    t_y = np.arange(len(Out_MatchedFilter)) / Fs
    t_samples = t_y[delay::ups]
    y_samples = Out_MatchedFilter[delay::ups]

    #  ##### Finding the comples Numbers in the Receiver X ########

    x = Out_MatchedFilter[delay:delay + N * ups:ups]  # every ups samples, the value of sQ is inserted into the sequence
    nl_shift = np.dot(np.transpose(np.conjugate(sQ)),sQ) / np.dot(np.transpose(np.conjugate(sQ)),x)
    x_shifted = x * nl_shift
    x = x_shifted

  
    delta = 0
    for ind in range(N):
        a1 = np.abs(x[ind])
        a2 = np.abs(sQ[ind])
        delta = delta - 1j * np.log((x[ind] * a2) / (sQ[ind] * a1))
    delta = delta / N
    x = x * np.exp(-1j * delta)

    axs = plt
    axs.scatter(x.real * np.sqrt(mod1.Es) / np.sqrt((10 ** (Pot_Inicial_x / 10)) / 1000),
                x.imag * np.sqrt(mod1.Es) / np.sqrt((10 ** (Pot_Inicial_x / 10)) / 1000), s=8, c='r', marker='x')
    # axs.scatter(x_test_NN.real*np.sqrt(mod1.Es)/np.sqrt((10**(Pot_Inicial_x/10))/1000), x_test_NN.imag*np.sqrt(mod1.Es)/np.sqrt((10**(Pot_Inicial_x/10))/1000), s=8, c='b', marker='x')
    axs.title('Training dataset', fontsize=9)
    plt.xlabel("Ix component Received")
    plt.ylabel("Qx component Received")
    plt.grid()
    plt.show()

    #  ####### Demapping and Decoding  X and Y #######
    n = len(x)

    Bit_receivedx = mod1.demodulate(x * np.sqrt(mod1.Es) / np.sqrt((10 ** (Pot_Inicial_x / 10)) / 1000), 'hard')
    Bit_decodedx = graytoBinary(Bit_receivedx, mod1.num_bits_symbol)  # decodificacao do gray


    print("________________", Pot_Inicial_x)
    qot = Quality()
    qot.BER(X, Bit_decodedx)
    print("BER X polarization with NN", qot.ber)

         
     """