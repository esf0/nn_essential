import sys
sys.path.append('/home/esf0/lib/FNFTpy')
sys.path.append('/home/esf0/PycharmProjects/nn_training')

import matplotlib.pyplot as plt
import matplotlib

import numpy as np
from FNFTpy import nsev
from FNFTpy import *


import nftpy
from importlib import reload
reload(nftpy)

import nft_analyse as na
reload(na)

print(nftpy.test())
print(nftpy.test2())
print(nftpy.test3())
print(nftpy.test4())


D = 1024
M = 4*D
Tmax = 15
tvec = np.linspace(-Tmax, Tmax, D)
# calculate suitable frequency bonds (xi)
rv, xi = nsev_inverse_xi_wrapper(D, tvec[0], tvec[-1], M)
xivec = xi[0] + np.arange(M) * (xi[1] - xi[0]) / (M - 1)

# temp_a = np.pi / (2. * Tmax) * (D-1)
# print(temp_a, temp_a - temp_a / M)
# print(xi[1] - xi[0])

# analytic field: chirp-free N=2.2 Satsuma-Yajima pulse
q = 2.2 / np.cosh(tvec)

# semi-analytic nonlinear spectrum
bound_states = np.array([0.7j, 1.7j])
# disc_norming_const_ana = [1.0, -1.0]
cont_b_ana = 0.587783 / np.cosh(xivec * np.pi) * np.exp(1.0j * np.pi)


# t = np.array([i * dt for i in range(len(signal))])
#
# n_signal = len(signal)
# xi_span = np.pi / (2. * dt) * 2
# dxi = xi_span / n_signal
# xi = np.linspace(-xi_span / 2. + dxi, xi_span / 2., n_signal)
#
# bsl = 2
# if calc_type == 1 or calc_type >= 22:
#     bsl = 1
#
# start = time.time()
# res = nsev(signal, t, M=n_signal, Xi1=xi[0], Xi2=xi[-1], bsl=bsl, dis=calc_type, kappa=1)

res = nsev(q, tvec, M=M, Xi1=xi[0], Xi2=xi[-1], kappa=1, cst=1, dst=2)
cont_a = res['cont_a']

temp_test = na.get_contour_phase_shift_adaptive(q, tvec, xivec, cont_a)
print(temp_test)
print(temp_test[0] / (2 * np.pi))
print(len(res['bound_states']))

na.find_spectrum_pjt(q, tvec, nftpy.calculate_a, [2, 1], xivec, cont_a)

dt = tvec[1] - tvec[0]

# nftpy.find_spectrum_pjt(q, dt, 2, 1)


result = nftpy.calculate_coefficients(q, dt, 0.7j, 2, 1)
print(result)

result = nftpy.calculate_coefficients(q, dt, 1.7j, 2, 1)
print(result)

a = []
b = []
r = []
diff = []

for i in range(len(xivec)):
    result = nftpy.calculate_coefficients(q, dt, xivec[i] + 0.0j, 2, 1)
    a.append(result[1])
    b.append(result[3])
    r.append(result[3] / result[1])
    diff.append(cont_b_ana[i] - result[3])
    # print("a:", result[1],
    #       "b:", result[3],
    #       "r:", result[3] / result[1],
    #       "r_real:", cont_b_ana[i],
    #       "diff:", abs(cont_b_ana[i] - result[3] / result[1]))


# matplotlib.rcParams.update({'font.size': 30})
#
# fig, axs = plt.subplots(2, 1, figsize=(15,15))
# axs[0].plot(xivec, np.absolute(a), 'blue', linewidth=7)
# axs[0].plot(xivec, np.absolute(b), 'red', linewidth=3)
# axs[0].plot(xivec, np.absolute(r), 'green', linewidth=3)
# # axs[0].set_xlim(-Tmax, Tmax)
# # axs[0].set_ylim(0, 0.5)
# axs[0].set_xlabel(r'$\xi$')
# axs[0].set_ylabel('abs')
# axs[0].grid(True)
#
# axs[1].plot(xivec, np.absolute(diff), 'blue', linewidth=5)
# # axs[1].set_xlim(-Tmax, Tmax)
# axs[1].set_xlabel(r'$\xi$')
# axs[1].set_ylabel('abs diff')
# axs[1].grid(True)
#
# fig.show()
# plt.show(block=True)

# ------------------------


n_vert, n_hor = 2**5, 2**10
a_map = np.zeros(n_vert * n_hor, dtype=complex)

real_min = -5
real_max = 5
imag_min = 0
imag_max = 10

xi_map = na.get_rect_filled(real_min + 1.0j * imag_min, real_max + 1.0j * imag_max, n_hor, n_vert)
for i in range(len(xi_map)):
    a_map[i] = nftpy.calculate_a(q, dt, xi_map[i], 2, 1)

a_phase = np.angle(a_map)

x = np.linspace(real_min, real_max, n_hor)
y = np.linspace(imag_min, imag_max, n_vert)
X, Y = np.meshgrid(x, y)
Z = a_phase.reshape(X.shape)

matplotlib.rcParams.update({'font.size': 30})

fig, axs = plt.subplots(1, 1, figsize=(15, 15))

levels = 100
cset = axs.contourf(X, Y, Z, levels = levels, cmap=plt.cm.viridis)
fig.colorbar(cset, shrink=1, aspect=20)
# axs.grid(True)
# axs.set_xlim(-1, 1)
# axs.set_ylim(0, 3)
axs.set_xlabel(r'Re($\xi$)')
axs.set_ylabel(r'$Im(\xi)$')


fig.show()
plt.show(block=True)