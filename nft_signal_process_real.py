from FNFTpy import nsev
from FNFTpy import nsev_inverse, nsev_inverse_xi_wrapper
import pandas as pd
from timeit import default_timer as timer
from tqdm import tqdm

import sys
import numpy as np
import random
from scipy.fft import fft, ifft, fftfreq, fftshift, ifftshift
from scipy.integrate import simps
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib

import signal_generation as sg
from importlib import reload
reload(sg)
from ssfm import fiber_propogate

import nft_analyse as nft
reload(nft)

import scipy.io

# slope for boundaries interpolation
def slope(x):
    if np.absolute(x) > 0:
        return np.exp(-np.power(0.01 * x, 2))
        # return 0.0
    else:
        return 0.0

def slope_t(t):
    return t

def get_process_interval(signal, n_symb, t_proc, skip=0):

    end_point = int(t_proc * n_symb)
    # print(end_point)
    return signal[skip:skip + end_point]

def get_sub_signal(signal, n_symb, t_symb, num_symb_proc, num_symb_skip, n_shift):

    t_proc = num_symb_proc * t_symb
    signal_cut = get_process_interval(signal, n_symb, t_proc, skip=n_shift + num_symb_skip * n_symb)
    np_signal = len(signal)
    dt = t_symb / n_symb
    t_vector = np.array([(i - np_signal / 2) * dt for i in range(np_signal)])
    t_cut = get_process_interval(t_vector, n_symb, t_proc, skip=n_shift + num_symb_skip * n_symb)

    return signal_cut, t_cut

def add_lateral_to_signal(signal, f_slope, t):

    dt = t[1] - t[0]
    n_add = sg.next_power_of_2(len(signal)) - len(signal) // 2
    signal_new = sg.add_lateral_function(signal, f_slope, dt, n = n_add)

    t_add = np.array([i * dt for i in range(n_add)])
    t_new = np.concatenate((t_add - t_add[-1] + t[0], t, t_add + t[-1]), axis=None)

    return signal_new, t_new

def do_nft(signal, t):

    np_signal = len(signal)
    np_spectrum = 2 * np_signal

    # calculate suitable frequency bonds (xi)
    rv, xi_bound = nsev_inverse_xi_wrapper(np_signal, t[0], t[-1], np_spectrum)
    xi_vector = xi_bound[0] + np.arange(np_spectrum) * (xi_bound[1] - xi_bound[0]) / (np_spectrum - 1)

    start = timer()
    res = nsev(signal, t, M=np_spectrum, Xi1=xi_bound[0], Xi2=xi_bound[1], K=1024, kappa=1, cst=2, dst=0, dis=2)
    end = timer()

    # cont_spec = res['cont_ref']
    # bound_states = res['bound_states']
    # disc_norm = res['disc_norm']

    return xi_vector, res, (end - start)



def get_spectrograms(signal, n_symb, t_symb, num_symbols, ns_proc, step, ns_skip, shift, file_name):

    dt = t_symb / n_symb
    num_symb_proc = ns_proc # number of processed symbols (main interval)
    num_symb_skip_shift = ns_skip # some initial shift from left side
    # step = num_symb_proc # step size

    # maximum dispersion distance in t_s
    num_symb_d_max = 128

    # number of processing steps
    proc_steps = (num_symbols - 2 * num_symb_skip_shift - 2 * num_symb_d_max) // step
    if proc_steps > 300:
        proc_steps = 300
    # temp_r = [i for i in range(proc_steps)]

    df = pd.DataFrame()

    for proc_iter in range(proc_steps):
    # for proc_iter in [temp_r[0], temp_r[1]]:

        # to do three intervals I have to take
        for num_symb_d in [0, num_symb_d_max // 2, num_symb_d_max]:

            num_symb_total = 2 * num_symb_d + num_symb_proc
            num_symb_skip_base = num_symb_d + num_symb_skip_shift # desire skip to the proc interval. Have to be bigger that num_symb_d
            num_symb_skip = num_symb_skip_base - num_symb_d + proc_iter * step

            signal_cut, t_cut = get_sub_signal(signal, n_symb, t_symb, num_symb_total, num_symb_skip, shift)

            ft_spectrum = fftshift(fft(signal_cut))
            ft_w = np.array([(i - len(signal_cut) / 2) * (2. * np.pi / len(signal_cut)) for i in range(len(signal_cut))])

            signal_cut, t_cut = add_lateral_to_signal(signal_cut, slope, t_cut)
            # print(len(signal_cut))

            xi, res, time = do_nft(signal_cut, t_cut)
            # print("n sol", num_symb_d, len(res['bound_states']))
            dxi = xi[1] - xi[0]

            l1_norm = sg.get_l1(signal_cut, dt)
            l2_norm = sg.get_energy(signal_cut, dt)
            e_discrete = 4 * np.sum(np.imag(res['bound_states']))
            e_cont = -1. / np.pi * np.sum(np.log(np.power(np.absolute(res['cont_a']), 2))) * dxi



            result_dict = {"signal": signal_cut, "t": t_cut, "res": res, "xi": xi,
                           "l1_norm": l1_norm, "l2_norm": l2_norm,
                           "e_discrete": e_discrete, "e_cont": e_cont,
                           "e_tot": e_discrete + e_cont,
                           "time": time,
                           'proc_steps': proc_steps,
                           "proc_iter": proc_iter,
                           'num_symbols': num_symbols,
                           'n_symb': n_symb,
                           't_symb': t_symb,
                           'num_symb_d': num_symb_d,
                           'num_symb_skip_base': num_symb_skip_base,
                           'num_symb_skip': num_symb_skip,
                           'num_symb_d_max': num_symb_d_max,
                           'num_symb_proc': num_symb_proc,
                           'num_symb_skip_shift': num_symb_skip_shift,
                           'step': step}

            # print(np.absolute(e_discrete + e_cont - l2_norm), l2_norm, np.absolute(e_discrete + e_cont - l2_norm) / l2_norm)

            df = df.append(result_dict, ignore_index=True)

    df.to_pickle(file_name + ".pkl")


p_dbm_temp = int(sys.argv[1])
# p_dbm_temp = 3

mat = scipy.io.loadmat('../data/signal_from_pedro/SSMF_SP_SC_[Train]_resolution_1km_30_X_50_Km_' + str(p_dbm_temp) + '_Pdbm_30_GBd_16_QAM.mat')

m_dt = mat['delta_t'][0][0]
sp_symb = mat['sps'][0][0]
t_dim = m_dt * sp_symb * 10 ** 12
t_dimless = 1.0
dt_dimless = t_dimless / sp_symb

beta2_mat = abs(sg.dispersion_to_beta2(16.8))
gamma_mat = 1.14 * 10**(-3)

print(mat['Q2factor'])

mat_signal = np.reshape(mat['Signal_x_before_fiber'], (1, -1))[0]


p_dbm_actual = sg.mw_to_dbm(sg.get_average_power(mat_signal, m_dt) * 1000)
p_nd_temp = sg.mw_to_nd(sg.dbm_to_mw(p_dbm_actual), t_symb=t_dim,
                        beta2=abs(sg.dispersion_to_beta2(16.8)),
                        gamma=1.14 * 10 ** (-3))

print(p_dbm_actual, p_nd_temp)

# That is how it should be
# mat_signal = mat_signal * (np.sqrt(gamma_mat / beta2_mat) * (t_dim / t_dimless))
# print(sg.mw_to_dbm(sg.nd_to_mw(sg.get_average_power(mat_signal, dt_dimless), t_symb=t_dim, beta2=beta2_mat, gamma=gamma_mat)))
# That is how i have to do
mat_signal = sg.set_average_power(mat_signal, dt=dt_dimless, power=p_nd_temp)
print(sg.mw_to_dbm(sg.nd_to_mw(sg.get_average_power(mat_signal, dt_dimless), t_symb=t_dim, beta2=beta2_mat, gamma=gamma_mat))) # should be equal to p_dbm_temp
# print(sg.get_average_power(mat_signal, dt_dimless))


get_spectrograms(mat_signal, sp_symb, t_dimless, num_symbols=2**16, ns_proc=32, step=32, ns_skip=128, shift=0,
                 file_name="nft_real_signal_spectogram_" + str(p_dbm_temp) + "dbm")
