# Information about data 

## Requirements and data read/load
Here's how you can save a pandas DataFrame to a Parquet file in Python:

First, install the pyarrow package, which provides support for Parquet:

```bash
pip install pyarrow
```

Save your DataFrame to a Parquet file:

```python
import pandas as pd

# Assuming df is your DataFrame
df.to_parquet('data.parquet', engine='pyarrow')
```

Now, to read the Parquet file in Julia, you can use the Parquet.jl and DataFrames.jl packages.

Install the packages in Julia:

```julia

using Pkg
Pkg.add("Parquet")
Pkg.add("DataFrames")
```

Read the Parquet file in Julia:

```julia

using Parquet
using DataFrames

data = DataFrame(read_parquet("data.parquet"))
```

## Data structure

**Dataframe 1:**

**Files:**

- None yet

This dataframe contains data related to an experimental process, and includes the following columns:

- `minus_m_1_orig_x_real`: Real part of the value at the index `n-1` of the original data before transmission. Data type: float.
- `point_orig_x_real`: Real part of the value at the index `n` of the original data before transmission. Data type: float.
- `plus_m_1_orig_x_real`: Real part of the value at the index `n+1` of the original data before transmission. Data type: float.
- `minus_m_1_orig_x_imag`: Imaginary part of the value at the index `n-1` of the original data before transmission. Data type: float.
- `point_orig_x_imag`: Imaginary part of the value at the index `n` of the original data before transmission. Data type: float.
- `plus_m_1_orig_x_imag`: Imaginary part of the value at the index `n+1` of the original data before transmission. Data type: float.
- `point_label`: A label indicating the label of the data point. Data type: string.
- `point_x_shifted_real`: Real part of the value at index `n` of the data after it has been received and shifted. Data type: float.
- `point_x_shifted_imag`: Imaginary part of the value at index `n` of the data after it has been received and shifted. Data type: float.

**Dataframe 2:**

**Files:**

- triplets_fitted_errorstat_wo_noise_1_p0_z1200.parquet

This dataframe contains data related to a two-dimensional Gaussian distribution, and includes the following columns:

- `mean0`: The mean value of the first dimension of the two-dimensional Gaussian distribution. Data type: float.
- `mean1`: The mean value of the second dimension of the two-dimensional Gaussian distribution. Data type: float.
- `cov00`: The variance of the first dimension of the two-dimensional Gaussian distribution. Data type: float.
- `cov01`: The covariance between the first and second dimensions of the two-dimensional Gaussian distribution. Data type: float.
- `cov11`: The variance of the second dimension of the two-dimensional Gaussian distribution. Data type: float.
- `left_point_real`: Real part of the value of the left point in a triplet. Data type: float.
- `left_point_imag`: Imaginary part of the value of the left point in a triplet. Data type: float.
- `central_point_real`: Real part of the value of the center point in a triplet. Data type: float.
- `central_point_imag`: Imaginary part of the value of the center point in a triplet. Data type: float.
- `right_point_real`: Real part of the value of the right point in a triplet. Data type: float.
- `right_point_imag`: Imaginary part of the value of the right point in a triplet. Data type: float.