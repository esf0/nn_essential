import numpy as np
from scipy.fft import fft, ifft, fftfreq, fftshift

import matplotlib.pyplot as plt

# SSFM code for solving the normalized NLS equation
# By G. P. Agrawal for the 6th edition of NLFO book
fiblen = 1 # fiber length (in units of L_D)
beta2 = -1  # sign of GVD parameter beta_2
N = 1  # soliton order

# N^2 = L_D / L_{NL} = gamma P_0 T_0^2 / |beta_2|

# --- set simulation parameters
nt = 1024  # FFT points
Tmax = 32  # window size
step_num = np.round(20 * fiblen * N ** 2)  # No. of z steps
deltaz = fiblen / step_num  # step size in z
dtau = (2 * Tmax) / nt  # step size in tau
tau = [(i - nt / 2) * dtau for i in range(nt)]  # time array # (-nt/2:nt/2-1)*dtau
omega = fftshift(
    [(i - nt / 2) * (np.pi / Tmax) for i in range(nt)])  # omega array # fft.fftshift(-nt/2:nt/2-1)*(pi/Tmax)
uu = [2.7 / np.cosh(i) for i in tau]  # sech pulse shape (can be modified)

# --- Plot input pulse shape and spectrum
temp = fftshift(ifft(uu));  # Fourier transform
spect = [np.abs(i) ** 2 for i in temp]  # input spectrum
spect = spect / np.max(spect)  # normalize
freq = fftshift(omega) / (2 * np.pi)  # freq. array

fig, axs = plt.subplots(2, 1)
axs[0].plot(tau, np.power(np.absolute(uu), 2))
axs[0].set_xlim(-5, 5)
axs[0].set_xlabel('Normalized Time')
axs[0].set_ylabel('Normalized Power')
axs[0].grid(True)

# subplot(2,1,1)
# plot(tau, abs(uu).^2, ’--k’)
# hold on
# axis([-5 5 0 inf])
# xlabel(’Normalized Time’)
# ylabel(’Normalized Power’)


axs[1].plot(freq, spect)
axs[1].set_xlim(-5, 5)
axs[1].set_xlabel('Normalized Frequency')
axs[1].set_ylabel('Spectral Power')
axs[1].grid(True)

fig.show()

# subplot(2,1,2)
# plot(freq, spect, ’--k’)
# hold on
# axis([-.5 .5 0 inf])
# xlabel(’Normalized Frequency’)
# ylabel(’Spectral Power’)

# ---store dispersive phase shifts to speedup code
dispersion = [np.exp(0.5j * beta2 * deltaz * i ** 2) for i in omega]  # phase factor # exp(0.5i*beta2*omega.^2*deltaz)
hhz = 1j * N ** 2 * deltaz  # nonlinear phase factor

# *********[ Beginning of MAIN Loop ]***********
# scheme: 1/2N -> D -> 1/2N; first half step nonlinear

temp = [i * np.exp(np.abs(i) ** 2 * hhz / 2) for i in uu]  # note hhz/2 # uu.*exp(abs(uu).^2.*hhz/2)
for n in range(step_num + 1):
    f_temp = ifft(temp) * dispersion
    uu = fft(f_temp)
    temp = [i * np.exp(np.abs(i) ** 2 * hhz) for i in uu]  # uu.*exp(abs(uu).^2.*hhz);

uu = np.multiply(temp, [np.exp(np.abs(i) ** 2 * hhz / 2) for i in uu])  # temp.*exp(-abs(uu).^2.*hhz/2); # Final field

# ***************[ End of MAIN Loop ]**************


# ----Plot output pulse shape and spectrum
temp = fftshift(ifft(uu))  # Fourier transform
spect = np.power(np.absolute(temp), 2)  # output spectrum
spect = spect / max(spect)  # normalize

fig_ssfm, axs_ssfm = plt.subplots(2, 1)
axs_ssfm[0].plot(tau, np.power(np.absolute(uu), 2), 'g')
axs_ssfm[0].set_xlim(-5, 5)
axs_ssfm[0].set_xlabel('Normalized Time')
axs_ssfm[0].set_ylabel('Normalized Power')
axs_ssfm[0].grid(True)

axs_ssfm[1].plot(freq, spect, 'g')
axs_ssfm[1].set_xlim(-5, 5)
axs_ssfm[1].set_xlabel('Normalized Frequency')
axs_ssfm[1].set_ylabel('Spectral Power')
axs_ssfm[1].grid(True)

fig_ssfm.show()
