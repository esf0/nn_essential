from FNFTpy import nsev
from FNFTpy import nsev_inverse, nsev_inverse_xi_wrapper
import pandas as pd
from timeit import default_timer as timer
from tqdm import tqdm

import numpy as np
import random
from scipy.fft import fft, ifft, fftfreq, fftshift, ifftshift
from scipy.integrate import simps
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib

import signal_generation as sg
from importlib import reload
reload(sg)
from ssfm import fiber_propogate

import nft_analyse as nft
reload(nft)

import scipy.io


# slope for boundaries interpolation
def slope(x):
    if np.absolute(x) > 0:
        return np.exp(-np.power(0.01 * x, 2))
        # return 0.0
    else:
        return 0.0

def slope_t(t):
    return t

def get_process_interval(signal, n_symb, t_proc, skip=0):

    end_point = int(t_proc * n_symb)
    # print(end_point)
    return signal[skip:skip + end_point]

def get_sub_signal(signal, n_symb, t_symb, num_symb_proc, num_symb_skip, n_shift):

    t_proc = num_symb_proc * t_symb
    signal_cut = get_process_interval(signal, n_symb, t_proc, skip=n_shift + num_symb_skip * n_symb)
    t_cut = get_process_interval(t_vector, n_symb, t_proc, skip=n_shift + num_symb_skip * n_symb)

    return signal_cut, t_cut

def add_lateral_to_signal(signal, f_slope, t):

    n_add = sg.next_power_of_2(len(signal)) - len(signal) // 2
    signal_new = sg.add_lateral_function(signal, f_slope, dt, n = n_add)

    t_add = np.array([i * dt for i in range(n_add)])
    t_new = np.concatenate((t_add - t_add[-1] + t[0], t, t_add + t[-1]), axis=None)

    return signal_new, t_new


def do_nft(signal, t):

    np_signal = len(signal)
    np_spectrum = 2 * np_signal

    # calculate suitable frequency bonds (xi)
    rv, xi_bound = nsev_inverse_xi_wrapper(np_signal, t_cut[0], t_cut[-1], np_spectrum)
    xi_vector = xi_bound[0] + np.arange(np_spectrum) * (xi_bound[1] - xi_bound[0]) / (np_spectrum - 1)

    start = timer()
    res = nsev(signal_cut, t_cut, M=np_spectrum, Xi1=xi_bound[0], Xi2=xi_bound[1], kappa=1, cst=2, dst=0, dis=2)
    end = timer()

    # cont_spec = res['cont_ref']
    # bound_states = res['bound_states']
    # disc_norm = res['disc_norm']

    return xi_vector, res, (end - start)


df_region = pd.DataFrame()


# create signal

mod_type = "16qam"
n_car = 1
t_symb = 1.0
n_symb = 4
dt = t_symb / n_symb
num_symbols = 2 ** 12
n_lateral = 32 * n_symb
# p_ave = 0.74
# p_ave = 0.05
roll_off = 0.01

num_symb_skip_shift = 32

for p_dbm in [-20., -10.] + [-10. + i_temp * (0 - (-10)) / 20 for i_temp in range(1, 20)] + [0., 1., 2., 3., 5.]:
    p_ave = round(sg.mw_to_nd(sg.dbm_to_mw(p_dbm), t_symb=14.8), 6)

    data = sg.gen_wdm_bit_sequence(num_symbols, mod_type, n_car)
    points = sg.get_constellation_point(data, mod_type)
    signal = sg.get_wdm_signal(data, t_symb=t_symb, n_symb=n_symb,
                            func=sg.srrcos, func_args=[t_symb, roll_off],
                            n_carriers=n_car, mod_type=mod_type, n_lateral=n_lateral)
    signal = sg.set_average_power(signal, dt, p_ave, n_symb * (num_symbols // 2 - 1))
    # print(len(signal), sg.next_power_of_2(len(signal)))
    # signal = sg.add_lateral(signal, 2**13 - len(signal) // 2)
    n_add = sg.next_power_of_2(len(signal)) - len(signal) // 2
    signal = sg.add_lateral(signal, n_add)

    # signal = sg.add_lateral(signal, 2**10)

    np_signal = len(signal)
    t_vector = np.array([(i - np_signal / 2) * dt for i in range(np_signal)])

    for num_symb_proc in [1, 4, 16, 32, 64, 128]:
        for num_symb_d_max in [128]:

            step = num_symb_proc
            proc_steps = (num_symbols - 2 * num_symb_skip_shift - 2 * num_symb_d_max) // step

            for proc_iter in range(proc_steps):
            # for proc_iter in [temp_r[0], temp_r[1]]:

                # to do three intervals I have to take
                for num_symb_d in [0, num_symb_d_max // 2, num_symb_d_max]:
                # for num_symb_d in [0]:
                    # num_symb_d = 256
                    num_symb_total = 2 * num_symb_d + num_symb_proc

                    num_symb_skip_base = num_symb_d + num_symb_skip_shift # desire skip to the proc interval. Have to be bigger that num_symb_d
                    num_symb_skip = num_symb_skip_base - num_symb_d + proc_iter * step

                    signal_cut, t_cut = get_sub_signal(signal, n_symb, t_symb, num_symb_total, num_symb_skip, n_add + n_lateral)

                    ft_spectrum = fftshift(fft(signal_cut))
                    ft_w = np.array([(i - len(signal_cut) / 2) * (2. * np.pi / len(signal_cut)) for i in range(len(signal_cut))])

                    signal_cut, t_cut = add_lateral_to_signal(signal_cut, slope, t_cut)
                    # print(len(signal_cut))

                    xi, res, time = do_nft(signal_cut, t_cut)
                    # print("n sol", num_symb_d, len(res['bound_states']))
                    dxi = xi[1] - xi[0]

                    l1_norm = sg.get_l1(signal_cut, dt)
                    l2_norm = sg.get_energy(signal_cut, dt)
                    e_discrete = 4 * np.sum(np.imag(res['bound_states']))
                    e_cont = -1. / np.pi * np.sum(np.log(np.power(np.absolute(res['cont_a']), 2))) * dxi



                    result_dict = {"signal": signal_cut, "t": t_cut, "res": res, "xi": xi,
                                   'ft_spectrum': ft_spectrum, 'ft_w': ft_w,
                                   "l1_norm": l1_norm, "l2_norm": l2_norm,
                                   "e_discrete": e_discrete, "e_cont": e_cont,
                                   "e_tot": e_discrete + e_cont,
                                   "e_lost": np.absolute(e_discrete + e_cont - l2_norm),
                                   "e_lost_rel": np.absolute(e_discrete + e_cont - l2_norm) / l2_norm,
                                   "time": time,
                                   'proc_steps': proc_steps,
                                   "proc_iter": proc_iter,
                                   'num_symbols': num_symbols,
                                   'n_symb': n_symb,
                                   't_symb': t_symb,
                                   'n_lateral': n_lateral,
                                   'p_ave': p_ave,
                                   'roll_off': roll_off,
                                   'mod_type': mod_type,
                                   'n_car': n_car,
                                   'num_symb_d': num_symb_d,
                                   'num_symb_skip_base': num_symb_skip_base,
                                   'num_symb_skip': num_symb_skip,
                                   'num_symb_d_max': num_symb_d_max,
                                   'num_symb_proc': num_symb_proc,
                                   'num_symb_skip_shift': num_symb_skip_shift,
                                   'step': step}

                    print(np.absolute(e_discrete + e_cont - l2_norm), l2_norm, np.absolute(e_discrete + e_cont - l2_norm) / l2_norm)

                    df_region = df_region.append(result_dict, ignore_index=True)

df_region.to_pickle("nft_continuous_signal_scan_region.pkl")
