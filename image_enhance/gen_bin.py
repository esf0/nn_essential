# Convert JPG to Binary

with open('image.jpg', 'rb') as f:
    img = f.read()

with open('image.bin', 'wb+') as f:
    f.write(img)