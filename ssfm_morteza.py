def fibre_1(u0,dt,Z,Kz,beta,gamma,alpha,Iter,tol):

    K=u0.shape[1]

    dz=Z/Kz

    t=np.array(np.arange(K),ndmin=2)*dt-dt

    w=np.fft.fftshift(np.array(2*pi/dt/K*np.arange(K),ndmin=2))-pi/dt

    dw=w[0,2]-w[0,1]

    w=w-dw

    ufft=np.fft.fft(u0)

    u1=u0

    halfstep = np.exp((-alpha/2 - 1j*beta*(w**2)/2)*dz/2)

    for n in range(Kz):

        print("Fibre--%s%% done!" %np.floor((n+1)/Kz*100), end="\r")

        uhalf=np.fft.ifft(ufft*halfstep)

        for nn in range(Iter):

            uv=uhalf*np.exp(-1j*gamma*(np.abs(u1)**2+np.abs(u0)**2)*dz/2)

            uv=np.fft.fft(uv)

            ufft=uv*halfstep

            uv=np.fft.ifft(ufft)

            if np.sqrt(np.sum(np.abs(uv-u1)**2))/np.sqrt(np.sum(np.abs(u1)**2))<tol:

                u1=uv

                break

            else:

                u1=uv

        u0=u1

    return u0