import qrcode
import qrcode.image.svg
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPDF, renderPM

# Link for website
input_data = "https://esf0.github.io"
# input_data = "https://github.com/esf0/hpcom"
# filename = 'github_hpcom'
filename = 'esf0_site'




#Creating an instance of qrcode
qr = qrcode.QRCode(
        version=1,
        box_size=10,
        border=5)
qr.add_data(input_data)
qr.make(fit=True)

# method = 'basic'
method = 'combined'
if method == 'basic':
    # Simple factory, just a set of rects.
    factory = qrcode.image.svg.SvgImage
elif method == 'fragment':
    # Fragment factory (also just a set of rects)
    factory = qrcode.image.svg.SvgFragmentImage
else:
    # Combined path factory, fixes white space that may occur when zooming
    factory = qrcode.image.svg.SvgPathImage

img = qrcode.make(input_data, image_factory=factory)
# img = qr.make_image(fill='black', back_color='white')
img.save(filename + '.svg')


drawing = svg2rlg(filename + '.svg')
renderPDF.drawToFile(drawing, filename + ".pdf")