import os
import csv
import numpy as np
# import matplotlib
# import matplotlib.pyplot as plt
from tqdm import tqdm
import math

from os.path import dirname, join as pjoin
import scipy as sp
import scipy.io as sio

import tensorflow as tf
from tensorflow import keras
from keras import callbacks
from keras import metrics
from keras.models import Sequential
from keras.layers import Activation, Dense, Reshape, Flatten, BatchNormalization, Conv1D

# from keras.metrics import categorical_crossentropy
# from keras.optimizers import adam_v2

# from skopt import gbrt_minimize, gp_minimize, load
# from skopt.utils import use_named_args
#
# from skopt.space import Real, Categorical, Integer
# from skopt.callbacks import CheckpointSaver

job_name = "polycoef_fixed_2"
# DRIVE_DIR = "/home/esf0/PycharmProjects/nn_essential/nn_nft_poly_coef/data/"
DRIVE_DIR = "/work/ec180/ec180/esedov/data_for_polycoef/"
checkpoint_filepath = DRIVE_DIR + 'model_' + job_name + '_checkpoint'
# skopt_checkpoint_file = "b_checkpoint_" + job_name + ".pkl"
# skopt_param_file = "bo_param_" + job_name + ".txt"
load_flag = False  # previous state

input_shape = (1024, 2)
output_shape = (1024, 2)

predict_type = 'real'  # 'real', 'imag', 'both'

patience = 1000
n_epochs = 50000
# n_params_max = 20_000_000
initial_batch_size = 8000

# tf.keras.losses.MeanSquaredError()
# tf.keras.losses.MeanSquaredLogarithmicError()
loss_function = tf.keras.losses.MeanSquaredError()
learning_rate = 1e-4

f_data_load = True
# f_data_save = False

p_ave = 0.5
run_number_list = [0, 1]

train_x = np.ndarray((0, 1024, 2))
train_y_a = np.ndarray((0, 1024, 2))
train_y_b = np.ndarray((0, 1024, 2))

test_x = np.ndarray((0, 1024, 2))
test_y_a = np.ndarray((0, 1024, 2))
test_y_b = np.ndarray((0, 1024, 2))

if f_data_load:
    for run_number in run_number_list:
        frac, whole = math.modf(p_ave)
        p_ave_name = '_p_' + str(int(whole)) + '_' + str(round(frac * 100))
        run_name = '_run_' + str(int(run_number))

        train_x_load = np.load(DRIVE_DIR + "data_polycoef_train_x" + p_ave_name + run_name + ".npy", allow_pickle=True)
        train_x = np.concatenate((train_x, train_x_load))
        # train_y_a = np.load(DRIVE_DIR + "data_polycoef_train_a" + p_ave_name + run_name + ".npy", allow_pickle=True)
        # train_y_b = np.load(DRIVE_DIR + "data_polycoef_train_b" + p_ave_name + run_name + ".npy", allow_pickle=True)

        del train_x_load

        # normalised
        train_y_a_load = np.load(DRIVE_DIR + "data_polycoef_train_a_normalised" + p_ave_name + run_name + ".npy",
                                 allow_pickle=True)
        # train_y_b_load = np.load(DRIVE_DIR + "data_polycoef_train_b_normalised" + p_ave_name + run_name + ".npy", allow_pickle=True)
        train_y_a = np.concatenate((train_y_a, train_y_a_load))
        # test_y_b = np.concatenate((test_y_b, train_y_b_load))

        del train_y_a_load

        test_x_load = np.load(DRIVE_DIR + "data_polycoef_test_x" + p_ave_name + run_name + ".npy", allow_pickle=True)
        test_x = np.concatenate((test_x, test_x_load))
        # test_y_a = np.load(DRIVE_DIR + "data_polycoef_test_a" + p_ave_name + run_name + ".npy", allow_pickle=True)
        # test_y_b = np.load(DRIVE_DIR + "data_polycoef_test_b" + p_ave_name + run_name + ".npy", allow_pickle=True)

        del test_x_load

        # normalised
        test_y_a_load = np.load(DRIVE_DIR + "data_polycoef_test_a_normalised" + p_ave_name + run_name + ".npy",
                                allow_pickle=True)
        test_y_a = np.concatenate((test_y_a, test_y_a_load))
        # test_y_b = np.load(DRIVE_DIR + "data_polycoef_test_b_normalised" + p_ave_name + run_name + ".npy", allow_pickle=True)

        del test_y_a_load

    print('data loaded')

# chose file to train and test values


if predict_type == 'real':
    train_y = train_y_a[:, :, 0]  # only real part
    test_y = test_y_a[:, :, 0]

elif predict_type == 'imag':
    train_y = train_y_a[:, :, 1]  # only imag part
    test_y = test_y_a[:, :, 1]

else:
    train_y = train_y_a  # both real and imag parts
    test_y = test_y_a

n_conv = 6
n_filters = [64, 64, 128, 128, 256, 256]  # number of filters
s_kernel = [3, 3, 5, 5, 5, 5]  # kernel sizes
stride = [1, 1, 2, 2, 1, 2]  # strides
dilation = [1, 1, 1, 1, 1, 1]

n_dense = 2
n_cell = [2048, 2048]
activation = ['relu', 'relu', 'relu', 'relu', 'relu', 'relu', 'relu', 'relu']

padding = 'valid'


def create_model(n_conv, n_filters, s_kernel, stride, dilation, n_dense, n_cell, activation, padding='valid',
                 input_shape=(1024, 2), output_shape=1024):
    # build convolutional neural network
    # first layer has to have input shape
    list_of_layers = [
        Conv1D(filters=n_filters[0], kernel_size=s_kernel[0], strides=stride[0], dilation_rate=dilation[0],
               activation=activation[0], padding=padding, input_shape=input_shape)]
    for k in range(1, n_conv):
        list_of_layers.append(
            Conv1D(filters=n_filters[k], kernel_size=s_kernel[k], strides=stride[k], dilation_rate=dilation[k],
                   activation=activation[k], padding=padding))
    list_of_layers.append(Flatten())
    for k in range(n_dense):
        list_of_layers.append(Dense(units=n_cell[k], activation=activation[k + n_conv]))

    if len(np.shape(output_shape)) == 0:
        list_of_layers.append(Dense(units=output_shape))  # output layer has to have output shape
    else:
        list_of_layers.append(Dense(units=(output_shape[0] * output_shape[1])))
        list_of_layers.append(Reshape(output_shape))
    model = Sequential(list_of_layers)

    return model


loss = 100.0

model_build_flag = False

# build convolutional neural network
try:
    model = create_model(n_conv, n_filters, s_kernel, stride, dilation, n_dense, n_cell, activation,
                         padding=padding, input_shape=input_shape, output_shape=output_shape)
    model_build_flag = True

except Exception as e:
    # print("Probably too big batch size", e)
    print("Cannot create model")

print('Model was created')

if model_build_flag:

    model.summary()

    metrics_list = [metrics.MeanSquaredError(), metrics.MeanSquaredLogarithmicError()]
    # metrics_list = []

    model.compile(loss=loss_function,  # keras.losses.mean_squared_error
                  optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate),  # keras.optimizers.Adam(lr=0.001)
                  metrics=metrics_list)

    batch_size = initial_batch_size
    flag_success = False

    while not flag_success:

        earlystopping = callbacks.EarlyStopping(monitor="loss",
                                                mode="auto",
                                                patience=patience,
                                                restore_best_weights=True)

        checkpoint = callbacks.ModelCheckpoint(filepath=checkpoint_filepath,
                                               save_weights_only=False,
                                               save_best_only=True,
                                               monitor='val_loss',
                                               mode='min')

        try:
            history_w_aug = model.fit(train_x, train_y,
                                      validation_data=(test_x, test_y),
                                      epochs=n_epochs,
                                      batch_size=batch_size, verbose=2,
                                      callbacks=[earlystopping, checkpoint])
        except Exception as e:
            print("Probably too big batch size", e)
            batch_size = int(batch_size / 2)
        else:
            flag_success = True

    predictions = model.predict(test_x)
    mse = tf.keras.losses.MeanSquaredError()
    loss = mse(test_y, predictions).numpy()

    model.save(DRIVE_DIR + 'model_' + job_name + '.h5')

print('Finished')
