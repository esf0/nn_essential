# import os
import numpy as np
import scipy as sp
# import tensorflow as tf
# import commpy
import pandas as pd
from datetime import datetime
import ssfm_gpu

# import matplotlib
# import matplotlib.pyplot as plt

# import signal_generation as sg
# import waveform_optimiser as wf
# import channel_model as ch
import hpcom

# data_dir = "/work/ec180/ec180/esedov/data_collection/"
# data_dir = "/home/esf0/PycharmProjects/nn_essential/benchmark/data/"
data_dir = 'C:/Users/190243539/PycharmProjects/nn_essential/dispersion_precompensation/data/'
job_name = 'precdc_w_noise_1'
# job_name = 'extended_w_noise_5'
# job_name = 'new_w_noise_1'
# job_name = 'test'

df = pd.DataFrame()

n_channels_list = [1]
# n_channels_list = [1, 3, 7]
# p_ave_dbm_list = np.arange(61) * 0.5 - 20
# p_ave_dbm_list = np.arange(31) * 1 - 20
# p_ave_dbm_list = [0]
# p_ave_dbm_list = [1, 2, 3, 4, 5, 6, 7, 8]
# p_ave_dbm_list = [0, 1, 2, 3, 4, 5, 6, 7, 8]
p_ave_dbm_list = [-1, -2, -3, -4, -5, 0, 1, 2, 3, 4, 5, 6]
# p_ave_dbm_list = [0, -1, -2, -3, -4, -5, -6, 7, 8, 9, 10]

n_span_list = [12]
# n_span_list = [10, 15, 20]
# n_span_list = [12, 13, 14, 15, 16, 17, 18, 19, 20]
# n_span_list = [38, 50, 75]
# n_span_list = np.arange(1, 21)
n_runs = 1

noise_figure_db = 4.5
# noise_figure_db = -200
z_span = 80

cdc_precompensation_list = [0.0, 0.5, 1.0]

print(p_ave_dbm_list)
print(n_span_list)


def custom_channel_model(channel, wdm, signal_x, signal_y, wdm_info, cdc_precompensation=0.0, channels_type='all'):
    dt = 1. / wdm['sample_freq']

    points_x_orig = wdm_info['points_x']
    points_y_orig = wdm_info['points_y']

    ft_filter_values = wdm_info['ft_filter_values_x']
    np_signal = len(signal_x)

    # precompensation of the dispersion
    z_span = channel['z_span']
    channel['z_span'] = channel['z_span'] * cdc_precompensation
    signal_x, signal_y = ssfm_gpu.propagation.dispersion_compensation_manakov(channel, signal_x, signal_y,
                                                                              1. / wdm['sample_freq'])
    channel['z_span'] = z_span  # return original z_span

    # e_signal_x = get_energy(signal_x, dt * np_signal)
    # e_signal_y = get_energy(signal_y, dt * np_signal)
    p_signal_x = hpcom.metrics.get_average_power(signal_x, dt)
    p_signal_y = hpcom.metrics.get_average_power(signal_y, dt)
    p_signal_correct = hpcom.signal.dbm_to_mw(wdm['p_ave_dbm']) / 1000 / 2 * wdm['n_channels']
    print("Average signal power (x / y): %1.7f / %1.7f (has to be close to %1.7f)" % (
    p_signal_x, p_signal_y, p_signal_correct))

    start_time = datetime.now()
    signal_x, signal_y = ssfm_gpu.propagation.propagate_manakov(channel, signal_x, signal_y, wdm['sample_freq'])
    end_time = datetime.now()
    time_diff = (end_time - start_time)
    execution_time = time_diff.total_seconds() * 1000
    print("propagation took", execution_time, "ms")

    # e_signal_x_prop = get_energy(signal_x, dt * np_signal)
    # e_signal_y_prop = get_energy(signal_y, dt * np_signal)

    # print("Signal energy before propagation (x / y):", e_signal_x, e_signal_y)
    # print("Signal energy after propagation (x / y):", e_signal_x_prop, e_signal_y_prop)
    # print("Signal energy difference (x / y):",
    #       np.absolute(e_signal_x - e_signal_x_prop),
    #       np.absolute(e_signal_y - e_signal_y_prop))

    # postcompensation of the dispersion
    channel['z_span'] = channel['z_span'] * (1.0 - cdc_precompensation)
    signal_x, signal_y = ssfm_gpu.propagation.dispersion_compensation_manakov(channel, signal_x, signal_y, 1. / wdm['sample_freq'])
    channel['z_span'] = z_span  # return original z_span

    samples_x = hpcom.signal.receiver_wdm(signal_x, ft_filter_values, wdm)
    samples_y = hpcom.signal.receiver_wdm(signal_y, ft_filter_values, wdm)

    # for k in range(wdm['n_channels']):
    #     samples_x[k], samples_y[k] = dispersion_compensation(channel, samples_x[k], samples_y[k], wdm['downsampling_rate'] / wdm['sample_freq'])

    # print(np.shape(samples_x))

    sample_step = int(wdm['upsampling'] / wdm['downsampling_rate'])

    if channels_type == 'all':

        points_x = []
        points_y = []

        points_x_shifted = []
        points_y_shifted = []

        for k in range(wdm['n_channels']):
            samples_x_temp = samples_x[k]
            samples_y_temp = samples_y[k]
            # print(np.shape(samples_x_temp[::sample_step]))
            points_x.append(samples_x_temp[::sample_step].numpy())
            points_y.append(samples_y_temp[::sample_step].numpy())

            nl_shift_x = hpcom.signal.nonlinear_shift(points_x[k], points_x_orig[k])
            points_x_shifted.append(points_x[k] * nl_shift_x)

            nl_shift_y = hpcom.signal.nonlinear_shift(points_y[k], points_y_orig[k])
            points_y_shifted.append(points_y[k] * nl_shift_y)

        mod_type = hpcom.modulation.get_modulation_type_from_order(wdm['m_order'])
        scale_constellation = hpcom.modulation.get_scale_coef_constellation(mod_type) / np.sqrt(wdm['p_ave'] / 2)

        points_x_found = []
        points_y_found = []

        ber_x = []
        ber_y = []
        q_x = []
        q_y = []
        for k in range(wdm['n_channels']):
            start_time = datetime.now()
            points_x_found.append(hpcom.modulation.get_nearest_constellation_points_unscaled(points_x_shifted[k], mod_type))
            print("search x took", (datetime.now() - start_time).total_seconds() * 1000, "ms")

            start_time = datetime.now()
            points_y_found.append(hpcom.modulation.get_nearest_constellation_points_unscaled(points_y_shifted[k], mod_type))
            print("search y took", (datetime.now() - start_time).total_seconds() * 1000, "ms")

            start_time = datetime.now()
            ber_x.append(hpcom.metrics.get_ber_by_points(points_x_orig[k] * scale_constellation, points_x_found[k], mod_type))
            print("ber x took", (datetime.now() - start_time).total_seconds() * 1000, "ms")

            start_time = datetime.now()
            ber_y.append( hpcom.metrics.get_ber_by_points(points_y_orig[k] * scale_constellation, points_y_found[k], mod_type))
            print("ber y took", (datetime.now() - start_time).total_seconds() * 1000, "ms")

            q_x.append(np.sqrt(2) * sp.special.erfcinv(2 * ber_x[k][0]))
            q_y.append(np.sqrt(2) * sp.special.erfcinv(2 * ber_y[k][0]))

            print("BER (x / y):", ber_x[k], ber_y[k])
            print(r'Q^2-factor (x / y):', q_x[k], q_y[k])

    elif channels_type == 'middle':

        k = (wdm['n_channels'] - 1) // 2

        samples_x_temp = samples_x[k]
        samples_y_temp = samples_y[k]

        points_x = samples_x_temp[::sample_step].numpy()
        points_y = samples_y_temp[::sample_step].numpy()

        nl_shift_x = hpcom.signal.nonlinear_shift(points_x, points_x_orig[k])
        points_x_shifted = points_x * nl_shift_x

        nl_shift_y = hpcom.signal.nonlinear_shift(points_y, points_y_orig[k])
        points_y_shifted = points_y * nl_shift_y

        mod_type = hpcom.modulation.get_modulation_type_from_order(wdm['m_order'])
        scale_constellation = hpcom.modulation.get_scale_coef_constellation(mod_type) / np.sqrt(wdm['p_ave'] / 2)

        points_x_found = hpcom.modulation.get_nearest_constellation_points_unscaled(points_x_shifted, mod_type)
        points_y_found = hpcom.modulation.get_nearest_constellation_points_unscaled(points_y_shifted, mod_type)

        ber_x = hpcom.metrics.get_ber_by_points(points_x_orig[k] * scale_constellation, points_x_found, mod_type)
        ber_y =  hpcom.metrics.get_ber_by_points(points_y_orig[k] * scale_constellation, points_y_found, mod_type)
        q_x = np.sqrt(2) * sp.special.erfcinv(2 * ber_x[0])
        q_y = np.sqrt(2) * sp.special.erfcinv(2 * ber_y[0])

    else:
        print('Error[full_line_model_wdm]: no such type of channels_type variable')

    result = {
        'points_x': points_x,
        'points_x_orig': points_x_orig,
        'points_x_shifted': points_x_shifted,
        'points_x_found': points_x_found,
        'points_y': points_y,
        'points_y_orig': points_y_orig,
        'points_y_shifted': points_y_shifted,
        'points_y_found': points_y_found,
        'ber_x': ber_x,
        'ber_y': ber_y,
        'q_x': q_x,
        'q_y': q_y
    }

    return result


for cdc_precompensation in cdc_precompensation_list:
    for n_channels in n_channels_list:
        for p_ave_dbm in p_ave_dbm_list:
            for n_span in n_span_list:
                for run in range(n_runs):
                    print(f'run = {run} / n_channels = {n_channels} / p_dbm = {p_ave_dbm} / n_span = {n_span}')
                    wdm_full = hpcom.signal.create_wdm_parameters(n_channels=n_channels, p_ave_dbm=p_ave_dbm, n_symbols=2 ** 16, m_order=16, roll_off=0.1, upsampling=16,
                                                   downsampling_rate=1, symb_freq=34e9, channel_spacing=75e9, n_polarisations=2, seed='time')

                    channel_full = hpcom.channel.create_channel_parameters(n_spans=n_span,
                                                           z_span=z_span,
                                                           alpha_db=0.2,
                                                           gamma=1.2,
                                                           noise_figure_db=noise_figure_db,
                                                           dispersion_parameter=16.8,
                                                           dz=1)

                    signal_x, signal_y, wdm_info = hpcom.signal.generate_wdm(wdm_full)

                    # result_channel = hpcom.channel.full_line_model_wdm(channel_full, wdm_full, channels_type='middle')
                    result_channel = custom_channel_model(channel_full, wdm_full, signal_x, signal_y, wdm_info,
                                                          cdc_precompensation=cdc_precompensation,
                                                          channels_type='middle')

                    result_dict = {}

                    # result_dict['wdm'] = wdm_full
                    # result_dict['channel'] = channel_full

                    result_dict['run'] = run
                    result_dict['n_channels'] = n_channels
                    result_dict['p_ave_dbm'] = p_ave_dbm
                    result_dict['z_km'] = n_span * 80

                    result_dict['cdc_precompensation'] = cdc_precompensation

                    result_dict['noise_figure_db'] = channel_full['noise_figure_db']
                    result_dict['gamma'] = channel_full['gamma']
                    result_dict['z_span'] = channel_full['z_span']
                    result_dict['dispersion_parameter'] = channel_full['dispersion_parameter']
                    result_dict['dz'] = channel_full['dz']

                    result_dict['points_x_orig'] = result_channel['points_x_orig']
                    result_dict['points_x'] = result_channel['points_x']
                    result_dict['points_x_shifted'] = result_channel['points_x_shifted']

                    result_dict['points_y_orig'] = result_channel['points_y_orig']
                    result_dict['points_y'] = result_channel['points_y']
                    result_dict['points_y_shifted'] = result_channel['points_y_shifted']

                    result_dict['ber_x'] = result_channel['ber_x']
                    result_dict['ber_y'] = result_channel['ber_y']

                    result_dict['q_x'] = result_channel['q_x']
                    result_dict['q_y'] = result_channel['q_y']

                    df = df.append(result_dict, ignore_index=True)

    df.to_pickle(data_dir + 'data_collected_' + job_name + '.pkl')

